<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     *
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=50)
     *
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255)
     *
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    private $verified;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $deleted_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Role")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=9)
     *
     *
     */
    private $post_code;

    /**
     * @ORM\Column(type="string", length=50)
     *
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=6)
     *
     */
    private $street_number;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderUser", mappedBy="user")
     */
    private $orderUsers;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $city;

    public function getRoles()
    {
        return array($this->role->getName());
    }
    public function getUsername()
    {
        return $this->email;
    }

    public function __construct()
    {
        $this->orderUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    public function getSalt()
    {
        return null;
    }
    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function getCreatedAt()
    {
        if ($this->created_at) {
            return $this->created_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }

    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUpdatedAt()
    {
        if ($this->updated_at) {
            return $this->updated_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }

    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function getVerified(): ?bool
    {
        return $this->verified;
    }

    public function setVerified(bool $verified): self
    {
        $this->verified = $verified;
        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;
        return $this;
    }

    public function getPostCode(): ?string
    {
        return $this->post_code;
    }

    public function setPostCode(string $post_code): self
    {
        $this->post_code = $post_code;
        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;
        return $this;
    }

    public function getStreetNumber(): ?string
    {
        return $this->street_number;
    }

    public function setStreetNumber(string $street_number): self
    {
        $this->street_number = $street_number;

        return $this;
    }

    /**
     * @return Collection|OrderUser[]
     */
    public function getOrderUsers(): Collection
    {
        return $this->orderUsers;
    }

    public function eraseCredentials() {

    }
    
    public function addOrderUser(OrderUser $orderUser): self
    {
        if (!$this->orderUsers->contains($orderUser)) {
            $this->orderUsers[] = $orderUser;
            $orderUser->setUser($this);
        }
        return $this;
    }

    public function removeOrderUser(OrderUser $orderUser): self
    {
        if ($this->orderUsers->contains($orderUser)) {
            $this->orderUsers->removeElement($orderUser);
            if ($orderUser->getUser() === $this) {
                $orderUser->setUser(null);
            }
        }
        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;
        return $this;
    }

    public function getDeletedAt()
    {
        if ($this->deleted_at) {
            return $this->deleted_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }

    public function removeUserOrders($date): self
    {
        foreach ($this->getOrderUsers() as $rder) {
            $order->setDeletedAt($date);
        }
        return $this;
    }

}
