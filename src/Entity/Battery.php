<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BatteryRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Battery
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $capacity;

    /**
     * @ORM\Column(type="string", length=40)
     *
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Smartphone", mappedBy="battery")
     *
     */
    private $smartphones;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $deleted_at;

    public function __construct()
    {
        $this->smartphones = new ArrayCollection();
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('type', new Assert\NotBlank());
        $metadata->addPropertyConstraint('capacity', new Assert\NotBlank());
        $metadata->addPropertyConstraint('capacity', new Assert\Length(array("min" => 4, "max" => 5)));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUpdatedAt()
    {
        if ($this->updated_at) {
            return $this->updated_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }

    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at->format('Y-m-d H:i:s');
        return $this;
    }

    /**
     * @return Collection|Smartphone[]
     */
    public function getSmartphones(): Collection
    {
        return $this->smartphones;
    }

    public function addSmartphone(Smartphone $smartphone): self
    {
        if (!$this->smartphones->contains($smartphone)) {
            $this->smartphones[] = $smartphone;
            $smartphone->setBattery($this);
        }
        return $this;
    }

    public function removeSmartphone(Smartphone $smartphone): self
    {
        if ($this->smartphones->contains($smartphone)) {
            $this->smartphones->removeElement($smartphone);
            if ($smartphone->getBattery() === $this) {
                $smartphone->setBattery(null);
            }
        }
        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }

}
