<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table(name="orders")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $tax;

    /**
     * @ORM\Column(type="string", length=30)
     *
     */
    private $status;

    /**
     * @ORM\Column(type="float")
     *
     */
    private $total_price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     */
    private $discount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Payment", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $payment;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderUser", mappedBy="order")
     *
     */
    private $orderUsers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderSmartphone", mappedBy="order")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $orderSmartphones;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $deleted_at;

    public function __construct()
    {
        $this->orderUsers = new ArrayCollection();
        $this->orderSmartphones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTax(): ?int
    {
        return $this->tax;
    }

    public function setTax(int $tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getTotalPrice(): ?float
    {
        return $this->total_price;
    }

    public function calcTotalPrice(): ?float
    {
        $totalPrice = 0;
        foreach ($this->getOrderSmartphones() as $product) {
            $totalPrice += $product->getSmartphone()->getPrice() * $product->getCount();
        }
        return $totalPrice;
    }

    public function setTotalPrice(float $total_price): self
    {
        $this->total_price = $total_price;

        return $this;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setDiscount(?int $discount): self
    {
        $this->discount = $discount;
        return $this;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): self
    {
        $this->payment = $payment;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at->format('Y-m-d H:i:s');
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return Collection|OrderUser[]
     */
    public function getOrderUsers(): Collection
    {
        return $this->orderUsers;
    }

    public function addOrderUser(OrderUser $orderUser): self
    {
        if (!$this->orderUsers->contains($orderUser)) {
            $this->orderUsers[] = $orderUser;
            $orderUser->setOrder($this);
        }
        return $this;
    }

    public function removeOrderUser(OrderUser $orderUser): self
    {
        if ($this->orderUsers->contains($orderUser)) {
            $this->orderUsers->removeElement($orderUser);
            if ($orderUser->getOrder() === $this) {
                $orderUser->setOrder(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|OrderSmartphone[]
     */
    public function getOrderSmartphones(): Collection
    {
        return $this->orderSmartphones;
    }

    public function addOrderSmartphone(OrderSmartphone $orderSmartphone): self
    {
        if (!$this->orderSmartphones->contains($orderSmartphone)) {
            $this->orderSmartphones[] = $orderSmartphone;
            $orderSmartphone->setOrder($this);
        }
        return $this;
    }

    public function removeOrderSmartphone(OrderSmartphone $orderSmartphone): self
    {
        if ($this->orderSmartphones->contains($orderSmartphone)) {
            $this->orderSmartphones->removeElement($orderSmartphone);
            if ($orderSmartphone->getOrder() === $this) {
                $orderSmartphone->setOrder(null);
            }
        }
        return $this;
    }

    public function deleteProductsFromOrder()
    {
        $date = new \DateTime(date('Y-m-d H:i:s'));
        foreach ($this->getOrderSmartphones() as $product) {
            $product->setDeletedAt($date);
        }
    }

    public function getDeletedAt()
    {
        if ($this->deleted_at) {
            return $this->deleted_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }

}
