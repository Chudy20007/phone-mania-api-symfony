<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProcessorRepository")
 */
class Processor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     *
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     *
     */
    private $threads_info;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $threads_count;

    /**
     * @ORM\Column(type="string", length=100)
     *
     */
    private $gpu_name;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Smartphone", mappedBy="processor")
     *
     */
    private $smartphones;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    public function __construct()
    {
        $this->smartphones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getThreadsInfo(): ?string
    {
        return $this->threads_info;
    }

    public function setThreadsInfo(string $threads_info): self
    {
        $this->threads_info = $threads_info;
        return $this;
    }

    public function getThreadsCount(): ?int
    {
        return $this->threads_count;
    }

    public function setThreadsCount(int $threads_count): self
    {
        $this->threads_count = $threads_count;
        return $this;
    }

    public function getGpuName(): ?string
    {
        return $this->gpu_name;
    }

    public function setGpuName(string $gpu_name): self
    {
        $this->gpu_name = $gpu_name;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return Collection|Smartphone[]
     */
    public function getSmartphones(): Collection
    {
        return $this->smartphones;
    }

    public function addSmartphone(Smartphone $smartphone): self
    {
        if (!$this->smartphones->contains($smartphone)) {
            $this->smartphones[] = $smartphone;
            $smartphone->setProcessor($this);
        }
        return $this;
    }

    public function removeSmartphone(Smartphone $smartphone): self
    {
        if ($this->smartphones->contains($smartphone)) {
            $this->smartphones->removeElement($smartphone);
            if ($smartphone->getProcessor() === $this) {
                $smartphone->setProcessor(null);
            }
        }
        return $this;
    }

    public function getDeletedAt()
    {
        if ($this->deleted_at) {
            return $this->deleted_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }

}
