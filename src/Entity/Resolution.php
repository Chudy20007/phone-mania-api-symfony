<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResolutionRepository")
 */
class Resolution
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $width;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $height;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Smartphone", mappedBy="resolution")
     *
     */
    private $smartphones;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $deleted_at;

    public function __construct()
    {
        $this->smartphones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;
        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(int $height): self
    {
        $this->height = $height;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return Collection|Smartphone[]
     */
    public function getSmartphones(): Collection
    {
        return $this->smartphones;
    }

    public function addSmartphone(Smartphone $smartphone): self
    {
        if (!$this->smartphones->contains($smartphone)) {
            $this->smartphones[] = $smartphone;
            $smartphone->setResolution($this);
        }
        return $this;
    }

    public function removeSmartphone(Smartphone $smartphone): self
    {
        if ($this->smartphones->contains($smartphone)) {
            $this->smartphones->removeElement($smartphone);
            if ($smartphone->getResolution() === $this) {
                $smartphone->setResolution(null);
            }
        }
        return $this;
    }

    public function getDeletedAt()
    {
        if ($this->deleted_at) {
            return $this->deleted_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }

}
