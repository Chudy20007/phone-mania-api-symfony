<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="roles")
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 */
class Role
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50,unique=true)
     *
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $deleted_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="role", orphanRemoval=true)
     *
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getCreatedAt()
    {
        if ($this->updated_at) {
            return $this->updated_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public function setCreatedAt(): self
    {
        $this->created_at = new \DateTime(date('Y-m-d H:i:s'));
        return $this;
    }

    public function getUpdatedAt()
    {
        if ($this->updated_at) {
            return $this->updated_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function getDeletedAt()
    {
        if ($this->deleted_at) {
            return $this->deleted_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setRole($this);
        }
        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($users)) {
            $this->users->removeElement($user);
            if ($user->getRole() === $this) {
                $user->setRole(null);
            }
        }
        return $this;
    }

    public function removeUsers($date): self
    {
        foreach ($this->users as $user) {
            $user->setDeletedAt($date);
        }
        return $this;
    }

}
