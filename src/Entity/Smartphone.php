<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SmartphoneRepository")
 */
class Smartphone
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     *
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    private $communication;

    /**
     * @ORM\Column(type="float", scale=2)
     *
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\File(
     * maxSize = "3072k",
     * maxSizeMessage = "Maksymalny rozmiar pliku to 3 MB!",
     * mimeTypes = { "image/png", "image/jpeg" },
     * mimeTypesMessage = "Wymagany format pliku jpg lub png"
     * )
     *
     */
    private $photo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Manufacturer", inversedBy="smartphones")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $manufacturer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Display", inversedBy="smartphones")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $display;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Camera", inversedBy="smartphones")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $camera;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Processor", inversedBy="smartphones")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $processor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\System", inversedBy="smartphones")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $system;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Battery", inversedBy="smartphones")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $battery;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ram", inversedBy="smartphones")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $ram;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Resolution", inversedBy="smartphones")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $resolution;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Memory", inversedBy="smartphones")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $memory;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $count;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SmartphoneAttachment", mappedBy="smartphone")
     *
     */
    private $smartphoneAttachments;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */

    private $updated_at;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $deleted_at;

    public function __construct()
    {
        $this->smartphoneAttachments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getCommunication(): ?string
    {
        return $this->communication;
    }

    public function setCommunication(?string $communication): self
    {
        $this->communication = $communication;
        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function setPhoto($photo): self
    {
        $this->photo = $photo;
        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    public function getDisplay(): ?Display
    {
        return $this->display;
    }

    public function setDisplay(?Display $display): self
    {
        $this->display = $display;
        return $this;
    }

    public function getCamera(): ?Camera
    {
        return $this->camera;
    }

    public function setCamera(?Camera $camera): self
    {
        $this->camera = $camera;
        return $this;
    }

    public function getProcessor(): ?Processor
    {
        return $this->processor;
    }

    public function setProcessor(?Processor $processor): self
    {
        $this->processor = $processor;
        return $this;
    }

    public function getSystem(): ?System
    {
        return $this->system;
    }

    public function setSystem(?System $system): self
    {
        $this->system = $system;
        return $this;
    }

    public function getBattery(): ?Battery
    {
        return $this->battery;
    }

    public function setBattery(?Battery $battery): self
    {
        $this->battery = $battery;
        return $this;
    }

    public function getRam(): ?Ram
    {
        return $this->ram;
    }

    public function setRam(?Ram $ram): self
    {
        $this->ram = $ram;
        return $this;
    }

    public function getResolution(): ?Resolution
    {
        return $this->resolution;
    }

    public function setResolution(?Resolution $resolution): self
    {
        $this->resolution = $resolution;
        return $this;
    }

    public function getMemory(): ?Memory
    {
        return $this->memory;
    }

    public function setMemory(?Memory $memory): self
    {
        $this->memory = $memory;
        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return Collection|SmartphoneAttachment[]
     */
    public function getSmartphoneAttachments(): Collection
    {
        return $this->smartphoneAttachments;
    }

    public function addSmartphoneAttachment(SmartphoneAttachment $smartphoneAttachment): self
    {
        if (!$this->smartphoneAttachments->contains($smartphoneAttachment)) {
            $this->smartphoneAttachments[] = $smartphoneAttachment;
            $smartphoneAttachment->setSmartphone($this);
        }
        return $this;
    }

    public function removeSmartphoneAttachment(SmartphoneAttachment $smartphoneAttachment): self
    {
        if ($this->smartphoneAttachments->contains($smartphoneAttachment)) {
            $this->smartphoneAttachments->removeElement($smartphoneAttachment);
            if ($smartphoneAttachment->getSmartphone() === $this) {
                $smartphoneAttachment->setSmartphone(null);
            }
        }
        return $this;
    }

    public function removeAttachments($date): self
    {
        foreach ($this->smartphoneAttachments as $attachment) {
            $attachment->setDeletedAt($date);
        }
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at->format('Y-m-d H:i:s');
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUpdatedAt()
    {
        if ($this->updated_at) {
            return $this->updated_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function getDeletedAt()
    {
        if ($this->deleted_at) {
            return $this->deleted_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }

    /*   public function getColor(): ?string
{
return $this->color;
}

public function setColor(string $color): self
{
$this->color = $color;

return $this;
}

 */
}
