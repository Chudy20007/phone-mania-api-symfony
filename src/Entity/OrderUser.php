<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderUserRepository")
 */
class OrderUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", inversedBy="orderUsers")
     * @ORM\JoinColumn(nullable=false)

     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="orderUsers")
     * @ORM\JoinColumn(nullable=false)

     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $deleted_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderSmartphone", mappedBy="order")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $orderSmartphones;

    public function __construct()
    {
        $this->orderSmartphones = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at->format('Y-m-d H:i:s');
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function getDeletedAt()
    {
        if ($this->deleted_at) {
            return $this->deleted_at->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }
    /**
     * @return Collection|OrderSmartphone[]
     */
    public function getOrderSmartphones(): Collection
    {
        return $this->orderSmartphones;
    }

    public function addOrderSmartphone(OrderSmartphone $orderSmartphone): self
    {
        if (!$this->orderSmartphones->contains($orderSmartphone)) {
            $this->orderSmartphones[] = $orderSmartphone;
            $orderSmartphone->setOrder($this);
        }
        return $this;
    }

    public function removeOrderSmartphone(OrderSmartphone $orderSmartphone): self
    {
        if ($this->orderSmartphones->contains($orderSmartphone)) {
            $this->orderSmartphones->removeElement($orderSmartphone);
            if ($orderSmartphone->getOrder() === $this) {
                $orderSmartphone->setOrder(null);
            }
        }
        return $this;
    }

}
