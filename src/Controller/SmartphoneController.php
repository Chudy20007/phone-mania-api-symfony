<?php

namespace App\Controller;

use App\Entity\Attachment;
use App\Entity\Smartphone;
use App\Entity\SmartphoneAttachment;
use App\Form\AttachmentType;
use App\Form\SmartphoneEditType;
use App\Form\SmartphoneType;
use App\Repository\AttachmentRepository;
use App\Repository\BatteryRepository;
use App\Repository\CameraRepository;
use App\Repository\DisplayRepository;
use App\Repository\ManufacturerRepository;
use App\Repository\MemoryRepository;
use App\Repository\ProcessorRepository;
use App\Repository\RamRepository;
use App\Repository\ResolutionRepository;
use App\Repository\SmartphoneRepository;
use App\Repository\SystemRepository;
use App\Service\FileUploader;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SmartphoneController extends AbstractController
{
    private $classMetadataFactory;
    private $encoder;
    private $normalizer;

    public function __destruct()
    {
        unset($this->encoder);
        unset($this->normalizer);
        unset($this->classMetadataFactory);
        gc_collect_cycles();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
        $this->em = null;
        gc_collect_cycles();
    }

    public function __construct()
    {
        $this->classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $this->encoder = new JsonEncoder();
        $this->normalizer = new ObjectNormalizer($this->classMetadataFactory);
    }

    public function photo($img)
    {
        try
        {
            $file = new File('../public/uploads/photos/' . $img);
        } catch (\Exception $exception) {
            $file = new File('../public/uploads/photos/no_photo.jpg');
        }
        return new BinaryFileResponse($file);
    }
    public function index(SmartphoneRepository $repository)
    {
        $products = $repository->findActiveRecords();
        $normalizedProducts = $this->dateNormalize($products);
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(array('data' => $normalizedProducts), 'json');
        unset($serializer);
        unset($products);
        unset($repository);
        return new Response($response, 200, ['Content-Type' => 'application/json', 'Access-Control-Allow-Origin' => '*']);
    }

    public function findProducts(SmartphoneRepository $repository, $value)
    {
        $products = $repository->findActiveRecordsByValue($value);
        $normalizedProducts = $this->dateNormalize($products);
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(array('data' => $normalizedProducts), 'json');
        unset($serializer);
        unset($products);
        unset($repository);
        return new Response($response, 200, ['Content-Type' => 'application/json', 'Access-Control-Allow-Origin' => '*']);
    }

    public function findProductsByManufacturer(Request $request, SmartphoneRepository $repository)
    {
        $manufacturers = json_decode($request->getContent(), true);
        $products = $repository->findActiveRecordsByManufacturers($manufacturers);
        $normalizedProducts = $this->dateNormalize($products);
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(array('data' => $normalizedProducts), 'json');
        unset($serializer);
        unset($products);
        unset($repository);
        return new Response($response, 200, ['Content-Type' => 'application/json', 'Access-Control-Allow-Origin' => '*']);
    }

    public function edit($id, SmartphoneRepository $repository,
        ProcessorRepository $processorRepository, ManufacturerRepository $manufacturerRepository,
        CameraRepository $cameraRepository, AttachmentRepository $attachmentRepository
        , BatteryRepository $batteryRepository, SystemRepository $systemRepository, ResolutionRepository $resolutionRepository,
        MemoryRepository $memoryRepository, RamRepository $ramRepository, DisplayRepository $displayRepository) {
        $smartphone = $repository->find($id);
        $processors = $processorRepository->findActiveRecords();
        $manufacturers = $manufacturerRepository->findActiveRecords();
        $cameras = $cameraRepository->findActiveRecords();
        $batteries = $batteryRepository->findActiveRecords();
        $resolutions = $resolutionRepository->findActiveRecords();
        $rams = $ramRepository->findActiveRecords();
        $systems = $systemRepository->findActiveRecords();
        $displays = $displayRepository->findActiveRecords();
        $memories = $memoryRepository->findActiveRecords();
        $this->getNormalizer()->setCircularReferenceHandler(function ($object, string $format = null, array $context = array()) {
            return $object;
        });
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' =>
            [
                'smartphone' => $smartphone,
                'processors' => $processors,
                'manufacturers' => $manufacturers,
                'cameras' => $cameras,
                'batteries' => $batteries,
                'resolutions' => $resolutions,
                'rams' => $rams,
                'systems' => $systems,
                'displays' => $displays,
                'memories' => $memories,
            ],
        ], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function create(SmartphoneRepository $repository,
        ProcessorRepository $processorRepository, ManufacturerRepository $manufacturerRepository,
        CameraRepository $cameraRepository, AttachmentRepository $attachmentRepository
        , BatteryRepository $batteryRepository, SystemRepository $systemRepository, ResolutionRepository $resolutionRepository,
        MemoryRepository $memoryRepository, RamRepository $ramRepository, DisplayRepository $displayRepository) {
        $smartphones = $repository->findActiveRecords();
        $processors = $processorRepository->findActiveRecords();
        $manufacturers = $manufacturerRepository->findActiveRecords();
        $cameras = $cameraRepository->findActiveRecords();
        $batteries = $batteryRepository->findActiveRecords();
        $resolutions = $resolutionRepository->findActiveRecords();
        $rams = $ramRepository->findActiveRecords();
        $systems = $systemRepository->findActiveRecords();
        $displays = $displayRepository->findActiveRecords();
        $memories = $memoryRepository->findActiveRecords();
        $this->getNormalizer()->setCircularReferenceHandler(function ($object, string $format = null, array $context = array()) {
            return $object;
        });
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' =>
            [
                'smartphones' => $smartphones,
                'processors' => $processors,
                'manufacturers' => $manufacturers,
                'cameras' => $cameras,
                'batteries' => $batteries,
                'resolutions' => $resolutions,
                'rams' => $rams,
                'systems' => $systems,
                'displays' => $displays,
                'memories' => $memories,
            ],
        ], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function show($id, SmartphoneRepository $repository)
    {
        $product = $repository->findProductByID($id);
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(array('data' => $product[0]), 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json', 'Access-Control-Allow-Origin' => '*']);
    }

    public function delete(Smartphone $smartphone, EntityManagerInterface $em)
    {
        $serializer = $this->get('serializer');
        if ($smartphone->getDeletedAt() !== null) {
            $response = $serializer->serialize(['message' => 'Produkt został usunięty wcześniej!'], 'json');
            return new Response($response, 405, ['Content-Type' => 'application/json']);
        }
        $smartphone->removeAttachments(new \DateTime(date('Y-m-d H:i:s')));
        $smartphone->setDeletedAt(new \DateTime(date('Y-m-d H:i:s')));
        $em->flush();
        $response = $serializer->serialize(['message' => 'Produkt usunięty pomyślnie!'], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function store(Request $request, SmartphoneRepository $repository,
        ProcessorRepository $processorRepository, ManufacturerRepository $manufacturerRepository,
        CameraRepository $cameraRepository, AttachmentRepository $attachmentRepository,
        BatteryRepository $batteryRepository, SystemRepository $systemRepository,
        ResolutionRepository $resolutionRepository, MemoryRepository $memoryRepository,
        RamRepository $ramRepository, DisplayRepository $displayRepository,
        EntityManagerInterface $em, FileUploader $fileUploader) {
        $data = $request->request->all();
        $date = new \DateTime(date('Y-m-d H:i:s'));
        $battery = $batteryRepository->findOneBy(['id' => $data['battery']]);
        $processor = $processorRepository->findOneBy(['id' => $data['processor']]);
        $manufacturer = $manufacturerRepository->findOneBy(['id' => $data['manufacturer']]);
        $camera = $cameraRepository->findOneBy(['id' => $data['camera']]);
        $battery = $batteryRepository->findOneBy(['id' => $data['battery']]);
        $resolution = $resolutionRepository->findOneBy(['id' => $data['resolution']]);
        $ram = $ramRepository->findOneBy(['id' => $data['ram']]);
        $system = $systemRepository->findOneBy(['id' => $data['system']]);
        $display = $displayRepository->findOneBy(['id' => $data['display']]);
        $memory = $memoryRepository->findOneBy(['id' => $data['memory']]);
        $file = $request->files->get('photo');
        $smartphone = new Smartphone();
        $smartphone->setName($data['name']);
        $smartphone->setBattery($battery);
        $smartphone->setCamera($camera);
        $smartphone->setMemory($memory);
        $smartphone->setRam($ram);
        $smartphone->setManufacturer($manufacturer);
        $smartphone->setResolution($resolution);
        $smartphone->setDisplay($display);
        $smartphone->setSystem($system);
        $smartphone->setProcessor($processor);
        $smartphone->setCreatedAt($date);
        $data['photo'] = $file;
        //$smartphone->setPhoto($request->files->get('photo'));
        $form = $this->createForm(SmartphoneType::class, $smartphone);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $smartphone->setPhoto(new UploadedFile($request->files->get('photo'), $_FILES['photo']['tmp_name']));
            $file = $smartphone->getPhoto();
            $fileName = $fileUploader->upload($file);
            $smartphone->setPhoto($fileName);
            $em->persist($smartphone);
            $i = 0;
            $date = new \DateTime(date('Y-m-d H:i:s'));
            do {
                $productAttachment = new Attachment();
                $form = $this->createForm(AttachmentType::class, $productAttachment);
                $selectedAttachment = null;
                $selectedAttachment['source'] = $request->files->get('source')[$i];
                $form->submit($selectedAttachment);
                if ($form->isSubmitted() && $form->isValid()) {
                    $productAttachment->setCreatedAt($date);
                    $productAttachment->setSource(new UploadedFile($request->files->get('source')[$i], $_FILES['source']['tmp_name'][$i]));
                    $file = $productAttachment->getSource();
                    $fileName = $fileUploader->upload($file);
                    $productAttachment->setSource($fileName);
                    $em->persist($productAttachment);
                    $productPivotAttachment = new SmartphoneAttachment();
                    $productPivotAttachment->setCreatedAt($date);
                    $productPivotAttachment->setSmartphone($smartphone);
                    $productPivotAttachment->setAttachment($productAttachment);
                    $em->persist($productPivotAttachment);
                } else {
                    $serializer = $this->get('serializer');
                    $response = $serializer->serialize(['message' => $this->getErrorMessages($form)], 'json');
                    return new Response($response, 422, ['Content-Type' => 'application/json']);
                }
                $i++;
            } while (isset($request->files->get('source')[$i]));
            $em->flush();
            $serializer = $this->get('serializer');
            $response = $serializer->serialize(['message' => 'Produkt utworzony pomyślnie'], 'json');
            return new Response($response, 200, ['Content-Type' => 'application/json', 'Access-Control-Allow-Origin' => '*']);
        }
        $serializer = $this->get('serializer');
        $response = $serializer->serialize(['message' => $this->getErrorMessages($form)], 'json');
        return new Response($response, 422, ['Content-Type' => 'application/json']);
    }

    public function update(Smartphone $smartphone, Request $request, SmartphoneRepository $smartphoneRepository,
        ProcessorRepository $processorRepository, ManufacturerRepository $manufacturerRepository,
        CameraRepository $cameraRepository, AttachmentRepository $attachmentRepository,
        BatteryRepository $batteryRepository, SystemRepository $systemRepository,
        ResolutionRepository $resolutionRepository, MemoryRepository $memoryRepository,
        RamRepository $ramRepository, DisplayRepository $displayRepository,
        EntityManagerInterface $em, FileUploader $fileUploader) {
        $data = json_decode($request->getContent(), true);
        $date = new \DateTime(date('Y-m-d H:i:s'));
        $smartphone->setUpdatedAt($date);
        $battery = $batteryRepository->findOneBy(['id' => $data['battery']]);
        $processor = $processorRepository->findOneBy(['id' => $data['processor']]);
        $manufacturer = $manufacturerRepository->findOneBy(['id' => $data['manufacturer']]);
        $camera = $cameraRepository->findOneBy(['id' => $data['camera']]);
        $battery = $batteryRepository->findOneBy(['id' => $data['battery']]);
        $resolution = $resolutionRepository->findOneBy(['id' => $data['resolution']]);
        $ram = $ramRepository->findOneBy(['id' => $data['ram']]);
        $system = $systemRepository->findOneBy(['id' => $data['system']]);
        $display = $displayRepository->findOneBy(['id' => $data['display']]);
        $memory = $memoryRepository->findOneBy(['id' => $data['memory']]);
        $smartphone->setName($data['name']);
        $smartphone->setBattery($battery);
        $smartphone->setCamera($camera);
        $smartphone->setMemory($memory);
        $smartphone->setRam($ram);
        $smartphone->setPrice($data['price']);
        $smartphone->setCount($data['count']);
        $smartphone->setManufacturer($manufacturer);
        $smartphone->setResolution($resolution);
        $smartphone->setDisplay($display);
        $smartphone->setSystem($system);
        $smartphone->setProcessor($processor);
        $smartphone->setUpdatedAt($date);
        $data['photo'] = null;
        $form = $this->createForm(SmartphoneEditType::class, $smartphone);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $serializer = $this->get('serializer');
            $response = $serializer->serialize(['message' => 'Produkt edytowany pomyślnie'], 'json');
            return new Response($response, 200, ['Content-Type' => 'application/json']);
        }
        $serializer = $this->get('serializer');
        $response = $serializer->serialize(['message' => $this->getErrorMessages($form)], 'json');
        return new Response($response, 422, ['Content-Type' => 'application/json']);
    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = array();
        foreach ($form->all() as $field) {
            if ($field->getErrors()->count() > 0) {
                $fieldName = $field->getName();
                // $errors[$fieldName] = [];
                foreach ($field->getErrors() as $error) {
                    array_push($errors, $error->getMessage());
                }
            }
        }
        return $errors;
    }

    private function dateNormalize($products)
    {
        $dateSerializer = new Serializer(array(new DateTimeNormalizer('Y-m-d H:i:s')));
        $i = 0;
        while (isset($products[$i])) {
            $products[$i]['created_at'] = $dateSerializer->normalize($products[$i]['created_at']);
            $products[$i]['updated_at'] = $dateSerializer->normalize($products[$i]['updated_at']);
            $products[$i]['deleted_at'] = $dateSerializer->normalize($products[$i]['deleted_at']);
            $i++;
        }
        return $products;
    }

    public function getEncoder()
    {
        return $this->encoder;
    }

    public function getNormalizer()
    {
        return $this->normalizer;
    }

    private function getClassMetadataFactory()
    {
        return $this->$classMetadataFactory;
    }

    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }

}
