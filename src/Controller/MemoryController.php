<?php

namespace App\Controller;

use App\Entity\Memory;
use App\Form\MemoryType;
use App\Repository\MemoryRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class MemoryController extends AbstractController
{
    private $classMetadataFactory;
    private $encoder;
    private $normalizer;

    public function __construct()
    {
        $this->classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $this->encoder = new JsonEncoder();
        $this->normalizer = new ObjectNormalizer($this->classMetadataFactory);
    }

    public function index(MemoryRepository $repository)
    {
        $memories = $repository->findRecords();
        $normalizedMemories = $this->dateNormalize($memories);
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' => $normalizedMemories], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function show($id, MemoryRepository $repository, Request $request)
    {
        $memory = $repository->find($id);
        $this->getNormalizer()->setCircularReferenceHandler(function ($object, string $format = null, array $context = array()) {
            return $object;
        });
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' => $memory], 'json', ['groups' => ['default']]);
        return new Response($response, 200, ['Content-Type' => 'application/json', 'Access-Control-Allow-Origin' => '*']);
    }

    public function create(Request $request, EntityManagerInterface $em)
    {
        $data = json_decode($request->getContent(), true);
        $date = new \DateTime(date('Y-m-d H:i:s'));
        $memory = new Memory();
        $form = $this->createForm(MemoryType::class, $memory);
        $memory->setCreatedAt($date);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($memory);
            $em->flush();
            $serializer = $this->get('serializer');
            $response = $serializer->serialize(['message' => 'Pamieć fash utworzona pomyślnie'], 'json');
            return new Response($response, 200, ['Content-Type' => 'application/json']);
        }
        $serializer = $this->get('serializer');
        $response = $serializer->serialize(['message' => $this->getErrorMessages($form)], 'json');
        return new Response($response, 422, ['Content-Type' => 'application/json']);
    }

    public function delete(Memory $memory, EntityManagerInterface $em)
    {
        $serializer = $this->get('serializer');
        if ($memory->getDeletedAt() !== null) {
            $response = $serializer->serialize(['message' => 'Pamięć została usunięta wcześniej!'], 'json');
            return new Response($response, 405, ['Content-Type' => 'application/json']);
        }
        $memory->setDeletedAt(new \DateTime(date('Y-m-d H:i:s')));
        $em->flush();
        $response = $serializer->serialize(['message' => 'Pamięć usunięta pomyślnie!'], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    private function dateNormalize($products)
    {
        $dateSerializer = new Serializer(array(new DateTimeNormalizer('Y-m-d H:i:s')));
        $i = 0;
        while (isset($products[$i])) {
            $products[$i]['created_at'] = $dateSerializer->normalize($products[$i]['created_at']);
            $products[$i]['updated_at'] = $dateSerializer->normalize($products[$i]['updated_at']);
            $products[$i]['deleted_at'] = $dateSerializer->normalize($products[$i]['deleted_at']);
            $i++;
        }
        return $products;
    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = array();
        foreach ($form->all() as $field) {
            if ($field->getErrors()->count() > 0) {
                $fieldName = $field->getName();
                // $errors[$fieldName] = [];
                foreach ($field->getErrors() as $error) {
                    array_push($errors, $error->getMessage());
                }
            }
        }
        return $errors;
    }

    public function getEncoder()
    {
        return $this->encoder;
    }

    public function getNormalizer()
    {
        return $this->normalizer;
    }

    public function getClassMetadataFactory()
    {
        return $this->$classMetadataFactory;
    }

}
