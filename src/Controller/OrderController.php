<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\OrderSmartphone;
use App\Entity\OrderUser;
use App\Form\OrderType;
use App\Repository\OrderRepository;
use App\Repository\OrderSmartphoneRepository;
use App\Repository\PaymentRepository;
use App\Repository\SmartphoneRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class OrderController extends AbstractController
{
    private $classMetadataFactory;
    private $encoder;
    private $normalizer;

    public function __construct()
    {
        $this->classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $this->encoder = new JsonEncoder();
        $this->normalizer = new ObjectNormalizer($this->classMetadataFactory);
    }

    public function userOrders(OrderRepository $repository, EntityManagerInterface $em)
    {
        $token = $this->get('security.token_storage')->getToken();
        if (null !== $token) {
            $user = $token->getUser();
        }
        $orders = $repository->findOrdersByUserID($user->getId());
        $normalizedOrders = $this->dateNormalize($orders);
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' => $normalizedOrders], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function store(Request $request, EntityManagerInterface $em, OrderSmartphoneRepository $orderRepository, PaymentRepository $paymentRepository, SmartphoneRepository $repository, \Swift_Mailer $mailer)
    {
        $order = new Order();
        $data = $this->prepareData($request);
        $form = $this->createForm(OrderType::class, $order);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($order);
            $date = new \DateTime(date('Y-m-d H:i:s'));
            $payment = $paymentRepository->find($data['payment']);
            $order->setCreatedAt($date);
            $order->setPayment($payment);
            $this->attachProductsToOrder($order, $data, $date, $em, $repository);
            $this->attachUserToOrder($order, $date, $em);
            $em->flush();
            // $createdOrder = $orderRepository->find($order->getId());
            $user = $this->getLoggedUser();
            $products = $orderRepository->findOrderProducts($order->getId());
            $msg = $this->sendEmailOrderAccepted($user, $order, $products, $mailer);
            if ($msg == true) {
                $serializer = $this->get('serializer');
                $response = $serializer->serialize(['message' => 'Zlecenie przyjęto do realizacji!'], 'json');
                return new Response($response, 200, ['Content-Type' => 'application/json', 'Access-Control-Allow-Origin' => '*']);
            } else {
                $serializer = $this->get('serializer');
                $response = $serializer->serialize(['message' => $msg], 'json');
                return new Response($response, 200, ['Content-Type' => 'application/json', 'Access-Control-Allow-Origin' => '*']);
            }
        }
        $serializer = $this->get('serializer');
        $response = $serializer->serialize(['message' => $this->getErrorMessages($form)], 'json');
        return new Response($response, 422, ['Content-Type' => 'application/json']);
    }

    public function index(OrderRepository $repository)
    {
        $orders = $repository->findRecords();
        $normalizedOrders = $this->dateNormalize($orders);
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' => $normalizer], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function deleteProductFromOrder($productID, OrderRepository $orderRepository, OrderSmartphoneRepository $orderSmartphoneRepository, EntityManagerInterface $em)
    {
        $selectedProductFromOrder = $orderSmartphoneRepository->find($productID);
        $order = $selectedProductFromOrder->getOrder();
        //$this->denyAccessUnlessGranted('deleteProductFromOrder', $order);
        $selectedProductFromOrder->setDeletedAt(new \DateTime(date('Y-m-d H:i:s')));
        $updatedPrice = $order->calcTotalPrice() - $selectedProductFromOrder->getCount() * $selectedProductFromOrder->getSmartphone()->getPrice();
        $order->setTotalPrice($updatedPrice);
        $em->persist($selectedProductFromOrder);
        $em->persist($order);
        $order = $orderRepository->findActiveProductsInOrder($order->getId());
        $em->flush();
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' => $order, 'message' => 'Produkt został usunięty ze zlecenia!'], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function show($id, OrderRepository $repository, Request $request)
    {
        $order = $repository->findActiveProductsInOrder($id);
        //$this->denyAccessUnlessGranted('view', $order);
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' => $order], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json', 'Access-Control-Allow-Origin' => '*']);
    }

    public function delete($id, OrderRepository $repository, EntityManagerInterface $em)
    {
        $order = $repository->find($id);
        // $this->denyAccessUnlessGranted('delete', $order);
        $order->deleteProductsFromOrder();
        $order->setDeletedAt(new \DateTime(date('Y-m-d H:i:s')));
        $em->flush();
        $serializer = $this->get('serializer');
        $response = $serializer->serialize(['message' => 'Zlecenie usunięte pomyślnie!'], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function getEncoder()
    {
        return $this->encoder;
    }

    public function getNormalizer()
    {
        return $this->normalizer;
    }

    private function dateNormalize($orders)
    {
        $dateSerializer = new Serializer(array(new DateTimeNormalizer('Y-m-d H:i:s')));
        $i = 0;
        while (isset($orders[$i])) {
            $orders[$i]['created_at'] = $dateSerializer->normalize($orders[$i]['created_at']);
            $orders[$i]['updated_at'] = $dateSerializer->normalize($orders[$i]['updated_at']);
            $orders[$i]['deleted_at'] = $dateSerializer->normalize($orders[$i]['deleted_at']);
            $i++;
        }
        return $orders;
    }

    public function getClassMetadataFactory()
    {
        return $this->$classMetadataFactory;
    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = array();
        foreach ($form->all() as $field) {
            if ($field->getErrors()->count() > 0) {
                $fieldName = $field->getName();
                // $errors[$fieldName] = [];
                foreach ($field->getErrors() as $error) {
                    array_push($errors, $error->getMessage());
                }
            }
        }
        return $errors;
    }

    private function attachProductsToOrder($order, $products, $date, $em, $smartphoneRepository)
    {
        $ids = array();
        for ($i = 0; $i < count($products['orderSmartphones']); $i++) {
            array_push($ids, $products['orderSmartphones'][$i]['id']);
        }
        $selectedProducts = $smartphoneRepository->findById($ids);
        $i = 0;
        foreach ($selectedProducts as $selectedProduct) {
            $product = new OrderSmartphone();
            $product->setCount($products['orderSmartphones'][$i]['count']);
            $product->setOrder($order);
            $product->setSmartphone($selectedProduct);
            $product->setCreatedAt($date);
            $em->persist($product);
            $i++;
        }
    }

    private function attachUserToOrder($order, $date, $em)
    {
        $orderCreatedBySelectedUser = new OrderUser();
        $token = $this->get('security.token_storage')->getToken();
        if (null !== $token) {
            $user = $token->getUser();
        }
        $orderCreatedBySelectedUser->setUser($user);
        $orderCreatedBySelectedUser->setOrder($order);
        $orderCreatedBySelectedUser->setCreatedAt($date);
        $em->persist($orderCreatedBySelectedUser);
    }

    private function checkIfProductsExistInOrder($data)
    {
        if (count($data['orderSmartphones']) == 0) {
            $serializer = $this->get('serializer');
            $response = $serializer->serialize(['message' => 'Twój koszyk jest pusty! Wybierz przynajmniej jeden produkt!'], 'json');
            return new Response($response, 422, ['Content-Type' => 'application/json']);
        }
    }

    public function sendEmailOrderAccepted($user, $order, $products, \Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Zlecenie zostało przyjęte do realizacji'))
            ->setFrom('rzuciakk12@gmail.com', 'Phone Mania')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'email/order_accepted.html.twig',
                    array(
                        'user' => $user,
                        'order' => $order,
                        'products' => $products,
                    )
                ),
                'text/html'
            );
        $mailer->send($message, $failures);
        return true;
    }

    private function getLoggedUser()
    {
        $token = $this->get('security.token_storage')->getToken();
        if (null !== $token) {
            $user = $token->getUser();
            return $user;
        }
        return null;
    }

    private function prepareData($request)
    {
        $data = json_decode($request->getContent(), true);
        $data['status'] = 'przyjęto';
        $data['tax'] = 23;
        $data['discount'] = 0;
        return $data;
    }

}
