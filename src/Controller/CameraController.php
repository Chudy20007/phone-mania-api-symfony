<?php

namespace App\Controller;

use App\Entity\Camera;
use App\Form\CameraType;
use App\Repository\CameraRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CameraController extends AbstractController
{
    private $classMetadataFactory;
    private $encoder;
    private $normalizer;

    public function __construct()
    {
        $this->classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $this->encoder = new JsonEncoder();
        $this->normalizer = new ObjectNormalizer($this->classMetadataFactory);
    }

    public function index(CameraRepository $repository)
    {
        $cameries = $repository->findRecords();
        $normalizedProducts = $this->dateNormalize($cameries);
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' => $normalizedProducts], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function create(Request $request, EntityManagerInterface $em)
    {
        $data = json_decode($request->getContent(), true);
        $date = new \DateTime(date('Y-m-d H:i:s'));
        $data['created_at'] = $date->format('Y-m-d H:i:s');
        $camera = new Camera();
        $form = $this->createForm(CameraType::class, $camera);
        $camera->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($camera);
            $em->flush();
            $serializer = $this->get('serializer');
            $response = $serializer->serialize(['message' => 'Aparat utworzony pomyślnie'], 'json');
            return new Response($response, 200, ['Content-Type' => 'application/json']);
        }
        $serializer = $this->get('serializer');
        $response = $serializer->serialize(['message' => $this->getErrorMessages($form)], 'json');
        return new Response($response, 422, ['Content-Type' => 'application/json']);
    }
    
    public function show($id, CameraRepository $repository, Request $request)
    {
        $camera = $repository->find($id);
        $this->getNormalizer()->setCircularReferenceHandler(function ($object, string $format = null, array $context = array()) {
            return $object;
        });
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' => $camera], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json', 'Access-Control-Allow-Origin' => '*']);
    }

    public function delete(Camera $camera, EntityManagerInterface $em)
    {
        $serializer = $this->get('serializer');
        if ($camera->getDeletedAt() !== null) {
            $response = $serializer->serialize(['message' => 'Aparat został usunięty wcześniej!'], 'json');
            return new Response($response, 405, ['Content-Type' => 'application/json']);
        }
        $camera->setDeletedAt(new \DateTime(date('Y-m-d H:i:s')));
        $em->flush();
        $response = $serializer->serialize(['message' => 'Aparat usunięty pomyślnie!'], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = array();
        foreach ($form->all() as $field) {
            if ($field->getErrors()->count() > 0) {
                $fieldName = $field->getName();
                // $errors[$fieldName] = [];
                foreach ($field->getErrors() as $error) {
                    array_push($errors, $error->getMessage());
                }
            }
        }
        return $errors;
    }
    
    private function dateNormalize($products)
    {
        $dateSerializer = new Serializer(array(new DateTimeNormalizer('Y-m-d H:i:s')));
        $i = 0;
        while (isset($products[$i])) {
            $products[$i]['created_at'] = $dateSerializer->normalize($products[$i]['created_at']);
            $products[$i]['updated_at'] = $dateSerializer->normalize($products[$i]['updated_at']);
            $products[$i]['deleted_at'] = $dateSerializer->normalize($products[$i]['deleted_at']);
            $i++;
        }
        return $products;
    }

    public function getEncoder()
    {
        return $this->encoder;
    }

    public function getNormalizer()
    {
        return $this->normalizer;
    }

    public function getClassMetadataFactory()
    {
        return $this->$classMetadataFactory;
    }

}
