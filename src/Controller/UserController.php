<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\VerifyUser;
use App\Form\UserType;
use App\Entity\ForgotPassword;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use App\Repository\VerifyUserRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class UserController extends AbstractController
{
    private $classMetadataFactory;
    private $encoder;
    private $normalizer;

    public function __construct()
    {
        $this->classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $this->encoder = new JsonEncoder();
        $this->normalizer = new ObjectNormalizer($this->classMetadataFactory);
    }

    public function index(UserRepository $repository)
    {
        $users = $repository->findRecords();
        $normalizedUsers = $this->dateNormalize($users);
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' => $normalizedUsers], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function show($id, UserRepository $repository, Request $request)
    {
        $user = $repository->find($id);
        $this->getNormalizer()->setCircularReferenceHandler(function ($object, string $format = null, array $context = array()) {
            return $object;
        });
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['data' => $user], 'json', ['groups' => ['default']]);
        return new Response($response, 200, ['Content-Type' => 'application/json', 'Access-Control-Allow-Origin' => '*']);
    }

    public function delete(User $user, EntityManagerInterface $em)
    {
        $serializer = $this->get('serializer');
        if ($user->getDeletedAt() !== null) {
            $response = $serializer->serialize(['data' => 'Użytkownik został usunięty wcześniej!'], 'json');
            return new Response($response, 405, ['Content-Type' => 'application/json']);
        }
        $user->removeUserOrders(new \DateTime(date('Y-m-d H:i:s')));
        $user->setDeletedAt(new \DateTime(date('Y-m-d H:i:s')));
        $em->flush();
        $response = $serializer->serialize(['data' => 'Użytkownik usunięty pomyślnie!'], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function myProfile()
    {
        $serializer = $this->get('serializer');
        $u = $this->get('security.token_storage')->getToken()->getUser();
        $response = $serializer->serialize(['data' => $u], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }

    public function create(Request $request, EntityManagerInterface $em, RoleRepository $roleRepository, EncoderFactoryInterface $encoderFactory, \Swift_Mailer $mailer)
    {
        $data = json_decode($request->getContent(), true);
        $date = new \DateTime(date('Y-m-d H:i:s'));
        $data['created_at'] = $date->format('Y-m-d H:i:s');
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $encoder = $encoderFactory->getEncoder($user);
            $salt = $this->generateUniqueToken(); // this should be different for every user
            $password = $encoder->encodePassword($data['password'], $salt);
            $role = $roleRepository->findOneBy(['name' => 'ROLE_USER']);
            $user->setSalt($salt);
            $user->setPassword($password);
            $user->setCreatedAt($date);
            $user->setVerified(false);
            $user->setRole($role);
            $row = new VerifyUser();
            $row->setUser($user);
            $row->setCreatedAt($date);
            $row->setToken($salt);
            $em->persist($row);
            $em->persist($user);
            $em->flush();
            $date = new \DateTime(date('Y-m-d H:i:s'));
            $serializer = $this->get('serializer');
            $response = $serializer->serialize(['message' => 'W ramach aktywacji konta został wysłany link na podany adres e-mail!'], 'json');
            return new Response($response, 200, ['Content-Type' => 'application/json']);
        } else {
            $serializer = $this->get('serializer');
            $response = $serializer->serialize(['message' => $this->getErrorMessages($form)], 'json');
            return new Response($response, 422, ['Content-Type' => 'application/json']);
        }
    }

    public function update(User $user, Request $request, EntityManagerInterface $em)
    {
        $data = json_decode($request->getContent(), true);
        $date = new \DateTime(date('Y-m-d H:i:s'));
        $user->setUpdatedAt($date);
        $form = $this->createForm(UserType::class, $user);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $serializer = $this->get('serializer');
            $response = $serializer->serialize(['data' => 'Użytkownik edytowany pomyślnie'], 'json');
            return new Response($response, 200, ['Content-Type' => 'application/json']);
        }
        $serializer = $this->get('serializer');
        $response = $serializer->serialize(['message' => $this->getErrorMessages($form)], 'json');
        return new Response($response, 422, ['Content-Type' => 'application/json']);
    }

    public function verifyAccount($token, EntityManagerInterface $em, VerifyUserRepository $verifyUserRepository)
    {
        $verifiedUserByToken = $verifyUserRepository->findOneBy(['token' => $token]);
        if ($verifiedUserByToken !== null) {
            $verifiedUserByToken->getUser()->setVerified(true);
            $em->remove($verifiedUserByToken);
            $em->flush();
            $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
            $response = $serializer->serialize(['message' => 'Konto zostało aktywowane!'], 'json', ['groups' => ['default']]);
            return new Response($response, 200, ['Content-Type' => 'application/json']);
        } else {
            $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
            $response = $serializer->serialize(['message' => 'Niepoprawna weryfikacja, błędny token!'], 'json');
            return new Response($response, 422, ['Content-Type' => 'application/json']);
        }
    }

    public function forgotPassword(Request $request, UserRepository $userRepository, EntityManagerInterface $em, \Swift_Mailer $mailer)
    {
        $data = json_decode($request->getContent(), true);
        $row = new ForgotPassword();
        $date = new \DateTime(date('Y-m-d H:i:s'));
        $user = $userRepository->findOneBy(['email' => $data['email']]);
        if ($user->getEmail() == $data['email']) {
            $token = $this->generateUniqueToken();
            $row->setUser($user);
            $row->setCreatedAt($date);
            $row->setToken($token);
            $em->persist($row);
            $em->flush();
;
            $this->getNormalizer()->setCircularReferenceHandler(function ($object, string $format = null, array $context = array()) {
                return $object;
            });
            $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
            $response = $serializer->serialize(['message' => 'Wysłano prośbę o zresetowanie hasła! Sprawdź swoją pocztę!'], 'json', ['groups' => ['default']]);
            return new Response($response, 200, ['Content-Type' => 'application/json']);
        }
        $this->getNormalizer()->setCircularReferenceHandler(function ($object, string $format = null, array $context = array()) {
            return $object;
        });
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['message' => 'Nieprawidłowy adres e-mail'], 'json');
        return new Response($response, 422, ['Content-Type' => 'application/json']);
    }
    private function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = array();
        foreach ($form->all() as $field) {
            if ($field->getErrors()->count() > 0) {
                $fieldName = $field->getName();
                // $errors[$fieldName] = [];
                foreach ($field->getErrors() as $error) {
                    array_push($errors, $error->getMessage());
                }
            }
        }
        return $errors;
    }

    public function resetPassword(Request $request, EntityManagerInterface $em, VerifyUserRepository $userRepository)
    {
        $data = json_decode($request->getContent(), true);
        $selectedToken = $userRepository->findOneBy(['token' => $data['token']]);
        if ($selectedToken && $selectedToken->getUser()->getEmail() === $data['email']) {
            $date = new \DateTime(date('Y-m-d H:i:s'));
            $user = $selectedToken->getUser();
            $user->setPassword($data['password']);
            $user->setUpdatedAt($date);
            $em->remove($selectedToken);
            $em->flush();
            $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
            $response = $serializer->serialize(['message' => 'Hasło zresetowane pomyślnie!'], 'json');
            return new Response($response, 200, ['Content-Type' => 'application/json']);
        }
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['message' => 'Niepoprawny token lub adres e-mail!'], 'json');
        return new Response($response, 422, ['Content-Type' => 'application/json']);
    }

    public function logout(Request $request)
    {
        $this->get('security.token_storage')->setToken(null);
        $serializer = new Serializer(array($this->getNormalizer()), array($this->getEncoder()));
        $response = $serializer->serialize(['message' => 'Wylogowano pomyślnie!'], 'json');
        return new Response($response, 200, ['Content-Type' => 'application/json', 'Authorization' => '']);
    }

    private function dateNormalize($products)
    {
        $dateSerializer = new Serializer(array(new DateTimeNormalizer('Y-m-d H:i:s')));
        $i = 0;
        while (isset($products[$i])) {
            $products[$i]['created_at'] = $dateSerializer->normalize($products[$i]['created_at']);
            $products[$i]['updated_at'] = $dateSerializer->normalize($products[$i]['updated_at']);
            $products[$i]['deleted_at'] = $dateSerializer->normalize($products[$i]['deleted_at']);
            $i++;
        }
        return $products;
    }

    private function generateUniqueToken()
    {
        return md5(uniqid());
    }

    public function getEncoder()
    {
        return $this->encoder;
    }

    public function getNormalizer()
    {
        return $this->normalizer;
    }

    public function getClassMetadataFactory()
    {
        return $this->$classMetadataFactory;
    }

}
