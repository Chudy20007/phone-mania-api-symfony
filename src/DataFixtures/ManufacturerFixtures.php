<?php

namespace App\DataFixtures;

use App\Entity\Manufacturer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class ManufacturerFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $manufacturer = new Manufacturer();
        $manufacturer->setName('Samsung');
        $manufacturer->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($manufacturer);
        $this->addReference('manufacturer', $manufacturer);

        $manufacturer = new Manufacturer();
        $manufacturer->setName('Motorola');
        $manufacturer->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($manufacturer);

        $manufacturer = new Manufacturer();
        $manufacturer->setName('Apple');
        $manufacturer->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($manufacturer);

        $manager->flush();
    }

    function getOrder()
    {
        return 6;
    }
}
