<?php

namespace App\DataFixtures;

use App\Entity\Processor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class ProcessorFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $processor = new Processor();
        $processor->setName('Qualcomm Snapdragon 425');
        $processor->setThreadsInfo('4 rdzenie, 1.40 GHz, Cortex A53');
        $processor->setThreadsCount(4);
        $processor->setGpuName('Adreno 308');
        $processor->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($processor);

        $processor = new Processor();
        $processor->setName('Qualcomm Snapdragon 625');
        $processor->setThreadsInfo('8 rdzeni, 2.0 GHz, Cortex A53');
        $processor->setThreadsCount(8);
        $processor->setGpuName('Adreno 506');
        $processor->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($processor);
        $this->addReference('processor', $processor);

        $processor = new Processor();
        $processor->setName('Qualcomm Snapdragon 821');
        $processor->setThreadsInfo('2 rdzenie, 2.35 GHz, Kryo + 2 rdzenie, 1.6 GHz, Kryo');
        $processor->setThreadsCount(4);
        $processor->setGpuName('Adreno 530');
        $processor->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($processor);

        $processor = new Processor();
        $processor->setName('MediaTek MT6750');
        $processor->setThreadsInfo('4 rdzenie, 1.5 GHz, Cortex A53 + 4 rdzenie, 1.0 GHz, Cortex A53');
        $processor->setThreadsCount(8);
        $processor->setGpuName('Mali-T860');
        $processor->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($processor);

        $manager->flush();
    }

    function getOrder()
    {
        return 9;
    }
}
