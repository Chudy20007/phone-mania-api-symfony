<?php

namespace App\DataFixtures;
use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
class RoleFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $role = new Role();
        $role->setName('ROLE_ADMIN');
        $role->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($role);
        $this->addReference('ROLE_ADMIN', $role);
        $role = new Role();
        $role->setName('ROLE_USER');
        $role->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($role);
        
        $role = new Role();
        $role->setName('ROLE_AUTHOR');
        $role->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($role);

        $role = new Role();
        $role->setName('ROLE_WORKER');
        $role->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($role);

        $manager->flush();
    }

    function getOrder()
    {
        return 1;
    }
}
