<?php

namespace App\DataFixtures;

use App\Entity\Display;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class DisplayFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $display = new Display();
        $display->setName('Super AMOLED');
        $display->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($display);
        $this->addReference('display', $display);

        $display = new Display();
        $display->setName('AMOLED');
        $display->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($display);

        $display = new Display();
        $display->setName('OLED');
        $display->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($display);

        $manager->flush();
    }

    function getOrder()
    {
        return 5;
    }
    
}
