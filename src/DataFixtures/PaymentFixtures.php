<?php

namespace App\DataFixtures;

use App\Entity\Payment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class PaymentFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $payment = new Payment();
        $payment->setName('płatność przy odbiorze');
        $payment->setPrice(30,00);
        $payment->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($payment);

        $payment = new Payment();
        $payment->setName('karta kredytowa');
        $payment->setPrice(15.00);
        $payment->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($payment);
        
        $payment = new Payment();
        $payment->setName('przelew');
        $payment->setPrice(15.00);
        $payment->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($payment);

        $manager->flush();
    }

    function getOrder()
    {
        return 8;
    }
}
