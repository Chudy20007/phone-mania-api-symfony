<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserFixtures extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFirstName('Admin');
        $user->setLastName('Admin');
        $user->setEmail('anonim410@gmail.com');
        $user->setRole($this->getReference('ROLE_ADMIN'));
        $user->setPostCode('58-200');
        $user->setStreet('Krzywa');
        $user->setStreetNumber('41');
        $user->setCity('Warszawa');
        $encoder = $this->container->get('security.password_encoder')->encodePassword($user, 'admin1');
        $user->setPassword($encoder);        
        $user->setVerified(true);
        $user->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($user);
        $manager->flush();
    }

    function getOrder()
    {
        return 2;
    }
}
