<?php

namespace App\DataFixtures;

use App\Entity\Camera;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class CameraFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $camera = new Camera();
        $camera->setFront('13 MP');
        $camera->setBack('13 MP');
        $camera->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($camera);
        $this->addReference('camera', $camera);

        $camera = new Camera();
        $camera->setFront('10 MP');
        $camera->setBack('13 MP');
        $camera->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($camera);        
        
        $camera = new Camera();
        $camera->setFront('5 MP');
        $camera->setBack('8 MP');
        $camera->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($camera);        
        
        $camera = new Camera();
        $camera->setFront('10 MP');
        $camera->setBack('10 MP');
        $camera->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($camera);        
        
        $camera = new Camera();
        $camera->setFront('10 MP');
        $camera->setBack('20 MP');
        $camera->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($camera);

        $manager->flush();
    }
    
    function getOrder()
    {
        return 4;
    }
}
