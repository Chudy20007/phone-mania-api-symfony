<?php

namespace App\DataFixtures;

use App\Entity\Memory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class MemoryFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $memory = new Memory();
        $memory->setSize(8);
        $memory->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($memory);

        $memory = new Memory();
        $memory->setSize(16);
        $memory->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($memory);
        $this->addReference('memory', $memory);

        $memory = new Memory();
        $memory->setSize(32);
        $memory->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($memory);

        $manager->flush();
    }

    function getOrder()
    {
        return 7;
    }
}
