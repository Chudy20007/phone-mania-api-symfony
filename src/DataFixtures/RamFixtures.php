<?php

namespace App\DataFixtures;

use App\Entity\Ram;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class RamFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $ram = new Ram();
        $ram->setSize(8);
        $ram->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($ram);

        $ram = new Ram();
        $ram->setSize(4);
        $ram->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($ram);

        $ram = new Ram();
        $ram->setSize(2);
        $ram->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($ram);
        $this->addReference('ram', $ram);

        $manager->flush();
    }

    function getOrder()
    {
        return 10;
    }
}
