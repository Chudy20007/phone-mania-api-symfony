<?php

namespace App\DataFixtures;

use App\Entity\Smartphone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class SmartphoneFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $smartphone = new Smartphone();
        $smartphone->setName('Galaxy A5');
        $smartphone->setPrice(1200.60);
        $smartphone->setBattery($this->getReference('battery'));
        $smartphone->setDisplay($this->getReference('display'));
        $smartphone->setCamera($this->getReference('camera'));
        $smartphone->setMemory($this->getReference('memory'));
        $smartphone->setRam($this->getReference('ram'));
        $smartphone->setResolution($this->getReference('resolution'));
        $smartphone->setSystem($this->getReference('system'));
        $smartphone->setManufacturer($this->getReference('manufacturer'));
        $smartphone->setProcessor($this->getReference('processor'));
        $smartphone->setCount(120);
        $smartphone->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($smartphone);

        $manager->flush();
    }

    function getOrder()
    {
        return 13;
    }
}
