<?php

namespace App\DataFixtures;

use App\Entity\Resolution;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class ResolutionFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $resolution = new Resolution();
        $resolution->setWidth(2960);
        $resolution->setHeight(1440);
        $resolution->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($resolution);

        $resolution = new Resolution();
        $resolution->setWidth(2160);
        $resolution->setHeight(1080);
        $resolution->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($resolution);

        $resolution = new Resolution();
        $resolution->setWidth(1920);
        $resolution->setHeight(1080);
        $resolution->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($resolution);
        $this->addReference('resolution', $resolution);

        $resolution = new Resolution();
        $resolution->setWidth(1280);
        $resolution->setHeight(720);
        $resolution->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($resolution);

        $manager->flush();
    }

    function getOrder()
    {
        return 11;
    }
}
