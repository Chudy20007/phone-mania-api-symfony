<?php

namespace App\DataFixtures;

use App\Entity\Battery;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class BatteryFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $battery = new Battery();
        $battery->setCapacity(3000);
        $battery->setType('Litowo-jonowa');
        $battery->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($battery);
        $this->addReference('battery', $battery);

        $battery = new Battery();
        $battery->setCapacity(2700);
        $battery->setType('Litowo-jonowa');
        $battery->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($battery);

        $battery = new Battery();
        $battery->setCapacity(3000);
        $battery->setType('Litowo-polimerowa');
        $battery->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($battery);

        $battery = new Battery();
        $battery->setCapacity(3300);
        $battery->setType('Litowo-polimerowa');
        $battery->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($battery);

        $manager->flush();
    }

    public function getOrder()
    {
        return 3; // number in which order to load fixtures
    }
}
