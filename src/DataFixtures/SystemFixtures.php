<?php

namespace App\DataFixtures;

use App\Entity\System;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class SystemFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $system = new System();
        $system->setName('Android');
        $system->setVersion('4.4.4 KitKat');
        $system->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($system);

        $system = new System();
        $system->setName('iOS');
        $system->setVersion('12.0');
        $system->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($system);

        $system = new System();
        $system->setName('Android');
        $system->setVersion('6.0.1 Marshmallow');
        $system->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($system);
        $this->addReference('system', $system);

        $system = new System();
        $system->setName('Android');
        $system->setVersion('8.0 Oreo');
        $system->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $manager->persist($system);

        $manager->flush();
    }

    function getOrder()
    {
        return 12;
    }
}
