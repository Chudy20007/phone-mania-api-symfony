<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181222103207 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE attachment (id INT AUTO_INCREMENT NOT NULL, source VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE battery (id INT AUTO_INCREMENT NOT NULL, capacity INT NOT NULL, type VARCHAR(40) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE camera (id INT AUTO_INCREMENT NOT NULL, front VARCHAR(10) NOT NULL, back VARCHAR(10) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE display (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE manufacturer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE memory (id INT AUTO_INCREMENT NOT NULL, size INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders (id INT AUTO_INCREMENT NOT NULL, payment_id INT NOT NULL, tax INT NOT NULL, status VARCHAR(30) NOT NULL, total_price DOUBLE PRECISION NOT NULL, discount INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_E52FFDEE4C3A3BB (payment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_smartphone (id INT AUTO_INCREMENT NOT NULL, order_id INT NOT NULL, smartphone_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_956A837B8D9F6D38 (order_id), INDEX IDX_956A837B2E4F4908 (smartphone_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_user (id INT AUTO_INCREMENT NOT NULL, order_id INT NOT NULL, user_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_C062EC5E8D9F6D38 (order_id), INDEX IDX_C062EC5EA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE processor (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, threads_info VARCHAR(100) NOT NULL, threads_count INT NOT NULL, gpu_name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ram (id INT AUTO_INCREMENT NOT NULL, size INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE resolution (id INT AUTO_INCREMENT NOT NULL, width INT NOT NULL, height INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_B63E2EC75E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE smartphone (id INT AUTO_INCREMENT NOT NULL, manufacturer_id INT NOT NULL, display_id INT NOT NULL, camera_id INT NOT NULL, processor_id INT NOT NULL, system_id INT NOT NULL, battery_id INT NOT NULL, ram_id INT NOT NULL, resolution_id INT NOT NULL, memory_id INT NOT NULL, name VARCHAR(100) NOT NULL, communication VARCHAR(100) DEFAULT NULL, price VARCHAR(10) NOT NULL, photo VARCHAR(255) DEFAULT NULL, count INT NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_26B07E2EA23B42D (manufacturer_id), INDEX IDX_26B07E2E51A2DF33 (display_id), INDEX IDX_26B07E2EB47685CD (camera_id), INDEX IDX_26B07E2E37BAC19A (processor_id), INDEX IDX_26B07E2ED0952FA5 (system_id), INDEX IDX_26B07E2E19A19CFC (battery_id), INDEX IDX_26B07E2E3366068 (ram_id), INDEX IDX_26B07E2E12A1C43A (resolution_id), INDEX IDX_26B07E2ECCC80CB3 (memory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE smartphone_attachment (id INT AUTO_INCREMENT NOT NULL, smartphone_id INT NOT NULL, attachment_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_510393752E4F4908 (smartphone_id), INDEX IDX_51039375464E68B (attachment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE system (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, version VARCHAR(30) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, role_id INT NOT NULL, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, verified TINYINT(1) NOT NULL, deleted_at DATETIME DEFAULT NULL, post_code VARCHAR(9) NOT NULL, street VARCHAR(50) NOT NULL, street_number VARCHAR(6) NOT NULL, city VARCHAR(50) NOT NULL, INDEX IDX_1483A5E9D60322AC (role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE4C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id)');
        $this->addSql('ALTER TABLE order_smartphone ADD CONSTRAINT FK_956A837B8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE order_smartphone ADD CONSTRAINT FK_956A837B2E4F4908 FOREIGN KEY (smartphone_id) REFERENCES smartphone (id)');
        $this->addSql('ALTER TABLE order_user ADD CONSTRAINT FK_C062EC5E8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE order_user ADD CONSTRAINT FK_C062EC5EA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE smartphone ADD CONSTRAINT FK_26B07E2EA23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id)');
        $this->addSql('ALTER TABLE smartphone ADD CONSTRAINT FK_26B07E2E51A2DF33 FOREIGN KEY (display_id) REFERENCES display (id)');
        $this->addSql('ALTER TABLE smartphone ADD CONSTRAINT FK_26B07E2EB47685CD FOREIGN KEY (camera_id) REFERENCES camera (id)');
        $this->addSql('ALTER TABLE smartphone ADD CONSTRAINT FK_26B07E2E37BAC19A FOREIGN KEY (processor_id) REFERENCES processor (id)');
        $this->addSql('ALTER TABLE smartphone ADD CONSTRAINT FK_26B07E2ED0952FA5 FOREIGN KEY (system_id) REFERENCES system (id)');
        $this->addSql('ALTER TABLE smartphone ADD CONSTRAINT FK_26B07E2E19A19CFC FOREIGN KEY (battery_id) REFERENCES battery (id)');
        $this->addSql('ALTER TABLE smartphone ADD CONSTRAINT FK_26B07E2E3366068 FOREIGN KEY (ram_id) REFERENCES ram (id)');
        $this->addSql('ALTER TABLE smartphone ADD CONSTRAINT FK_26B07E2E12A1C43A FOREIGN KEY (resolution_id) REFERENCES resolution (id)');
        $this->addSql('ALTER TABLE smartphone ADD CONSTRAINT FK_26B07E2ECCC80CB3 FOREIGN KEY (memory_id) REFERENCES memory (id)');
        $this->addSql('ALTER TABLE smartphone_attachment ADD CONSTRAINT FK_510393752E4F4908 FOREIGN KEY (smartphone_id) REFERENCES smartphone (id)');
        $this->addSql('ALTER TABLE smartphone_attachment ADD CONSTRAINT FK_51039375464E68B FOREIGN KEY (attachment_id) REFERENCES attachment (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9D60322AC FOREIGN KEY (role_id) REFERENCES roles (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE smartphone_attachment DROP FOREIGN KEY FK_51039375464E68B');
        $this->addSql('ALTER TABLE smartphone DROP FOREIGN KEY FK_26B07E2E19A19CFC');
        $this->addSql('ALTER TABLE smartphone DROP FOREIGN KEY FK_26B07E2EB47685CD');
        $this->addSql('ALTER TABLE smartphone DROP FOREIGN KEY FK_26B07E2E51A2DF33');
        $this->addSql('ALTER TABLE smartphone DROP FOREIGN KEY FK_26B07E2EA23B42D');
        $this->addSql('ALTER TABLE smartphone DROP FOREIGN KEY FK_26B07E2ECCC80CB3');
        $this->addSql('ALTER TABLE order_smartphone DROP FOREIGN KEY FK_956A837B8D9F6D38');
        $this->addSql('ALTER TABLE order_user DROP FOREIGN KEY FK_C062EC5E8D9F6D38');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEE4C3A3BB');
        $this->addSql('ALTER TABLE smartphone DROP FOREIGN KEY FK_26B07E2E37BAC19A');
        $this->addSql('ALTER TABLE smartphone DROP FOREIGN KEY FK_26B07E2E3366068');
        $this->addSql('ALTER TABLE smartphone DROP FOREIGN KEY FK_26B07E2E12A1C43A');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9D60322AC');
        $this->addSql('ALTER TABLE order_smartphone DROP FOREIGN KEY FK_956A837B2E4F4908');
        $this->addSql('ALTER TABLE smartphone_attachment DROP FOREIGN KEY FK_510393752E4F4908');
        $this->addSql('ALTER TABLE smartphone DROP FOREIGN KEY FK_26B07E2ED0952FA5');
        $this->addSql('ALTER TABLE order_user DROP FOREIGN KEY FK_C062EC5EA76ED395');
        $this->addSql('DROP TABLE attachment');
        $this->addSql('DROP TABLE battery');
        $this->addSql('DROP TABLE camera');
        $this->addSql('DROP TABLE display');
        $this->addSql('DROP TABLE manufacturer');
        $this->addSql('DROP TABLE memory');
        $this->addSql('DROP TABLE orders');
        $this->addSql('DROP TABLE order_smartphone');
        $this->addSql('DROP TABLE order_user');
        $this->addSql('DROP TABLE payment');
        $this->addSql('DROP TABLE processor');
        $this->addSql('DROP TABLE ram');
        $this->addSql('DROP TABLE resolution');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE smartphone');
        $this->addSql('DROP TABLE smartphone_attachment');
        $this->addSql('DROP TABLE system');
        $this->addSql('DROP TABLE users');
    }
}
