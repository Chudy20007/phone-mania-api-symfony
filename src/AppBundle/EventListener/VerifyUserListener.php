<?php

namespace App\AppBundle\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Entity\VerifyUser;

class VerifyUserListener
{
    private $appURL;
    private $mailer;
    private $templating;
    private $appEmail;

    public function __construct($appURL, $appEmail, \Swift_Mailer $mailer, \Twig_Environment $templating)
    {
        $this->appURL = $appURL;
        $this->appEmail =$appEmail;
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function postPersist(LifecycleEventArgs $args)
    {      
        $entity = $args->getObject();
        if ($entity instanceof VerifyUser) {
            $this->sendEmailWithToken($entity->getUser(),$entity->getToken(),$this->mailer);
            return;
        }
    }

    public function sendEmailWithToken($user, $token, $mailer)
    {
        $appURL = $this->appURL;
        $appEmail = $this->appEmail;
        $message = (new \Swift_Message('Aktywacja konta'))
            ->setFrom($appEmail, 'Phone Mania')
            ->setTo($user->getEmail())
            ->setBody(
                $this->templating->render(
                    'email/verify_user.html.twig',
                    array(
                        'token' => $token,
                        'user' => $user,
                        'appURL' => $appURL,
                    )
                ),
                'text/html'
            );
        $mailer->send($message);
        return true;
    }
}