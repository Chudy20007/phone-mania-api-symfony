<?php
namespace App\AppBundle\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;


/**
 * @param AuthenticationSuccessEvent $event
 */
class AuthenticationListener
{

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();
    
        $event->setData([
            'role' => $user->getRoles(),
            'token' => $event->getData()['token'],
            'message'=> 'Użytkownik zalogowany pomyślnie!',
        ]);
    }

    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        $data = [
            'message' => array('Niepoprawny login lub hasło!!'),
        ];

        $response = new JsonResponse($data, 403);

        $event->setResponse($response);
    }


    /** 
     * @param JWTNotFoundEvent $event
     */
    public function onJWTNotFound(JWTNotFoundEvent $event)
    {
        $data = [
            'message' => array('Brak dostępu do zasobu!'),
        ];

        $response = new JsonResponse($data, 403);

        $event->setResponse($response);
    }


    /**
     * @param JWTExpiredEvent $event
     */
    public function onJWTExpired(JWTExpiredEvent $event)
    {
        /** @var JWTAuthenticationFailureResponse */
        $response = $event->getResponse();

        $response->setMessage('Twoja sesja wygasła!');
    }
}
