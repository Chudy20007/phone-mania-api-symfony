<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;

class FileUploader
{
    private $photosDirectory;

    public function __construct($photosDirectory)
    {
        $this->photosDirectory = $photosDirectory;
    }

    public function upload(UploadedFile $file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        try {
            
            $file->move($this->getPhotosDirectory(), $fileName);

            $image = Image::make($this->getPhotosDirectory().'/'.$fileName)
            ->resize(250, 300)
            ->save($this->getPhotosDirectory().'/'.$fileName);
        } catch (FileException $e) {
            throw new FileException("Nie znaleziono danego zasobu!");
        }

        return $fileName;
    }

    public function getSelectedPhoto($fileName)
    {
        return $this->getPhotosDirectory().'/'.$fileName;
    }
    public function getPhotosDirectory()
    {
        return $this->photosDirectory;
    }
}