<?php

namespace App\Form;

use App\Entity\System;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Constraints\Regex;

class SystemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = array(
            'required' => true,
        );
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole nazwa systemu nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/',
                        'message' => 'Niepoprawny nazwa dla systemu!',
                    ]),
                ],
            ])
            ->add('version', TextType::class, [
                'required' => true,
                'constraints' => [
                new NotBlank([
                    'message' => "Pole wersja nie powinno być puste!",
                ]),
                new Regex([
                    'pattern' => '/[A-ZĄĘŹŻŁÓŁa-ząęźżół0-9.]{3,}/',
                    'message' => 'Niepoprawny wersja dla systemu!',
                ]),
                ]
            ])
            ->add('save', SubmitType::class, array('label' => 'Utwórz'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => System::class,
            'csrf_protection' => false,
            'allow_extra_fields'=> true,
            'constraints' => new UniqueEntity([
                'fields' => ['name', 'version'],
                'message' => 'System operacyjny o takiej wersji już istnieje!',
            ]),
        ]);
    }
}
