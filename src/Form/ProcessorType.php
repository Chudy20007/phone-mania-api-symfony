<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Constraints\Regex;
use App\Entity\Processor;

class ProcessorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = array(
            'required' => true,
        );
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole nazwa procesora nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]{3,}\s*[a-zA-ZZąćęłńóśźżĄĆĘŁŃÓŚŹŻ0-9]{1,}/',
                        'message' => 'Niepoprawna nazwa dla procesora!',
                    ]),
                ],
            ])
            ->add('gpu_name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole karty graficznej nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/[A-Za-z]{3,4}/',
                        'message' => 'Niepoprawna nazwa dla karty graficznej!',
                    ]),
                ],
            ])
            ->add('threads_info', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole informacje o rdzeniach procesora nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/[A-Za-z]{3,4}/',
                        'message' => 'Niepoprawna informacje o rdzeniach procesora!',
                    ]),
                ],
            ])
            ->add('threads_count', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Podałeś niewłaściwą ilość (minimum %num% znaki) rdzeni dla procesora!',
                'invalid_message_parameters' => array('%num%' => 1),
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole ilość rdzeni nie powinno być puste!",
                    ]),                
                ],
            ])
            ->add('save', SubmitType::class, array('label' => 'Utwórz'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Processor::class,
            'csrf_protection' => false,
            'constraints' => new UniqueEntity([
                'fields' => ['name'],
                'message' => 'Taki rodzaj procesora już istnieje!',
            ]),
        ]);
    }
}
