<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Constraints\Regex;
use App\Entity\Payment;

class PaymentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = array(
            'required' => true,
        );
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole metoda płatności nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/[A-Za-ząęóźżćłĄĘŹŻĆŁÓ ]{4,}/',
                        'message' => 'Niepoprawna nazwa płatności!',
                    ]),
                ],
            ])
            ->add('created_at', DateTimeType::class, [
                'invalid_message' => "Podana data jest niepoprawna!",
                'format' => 'yyyy-MM-dd HH:mm:ss',
                'input' => 'datetime',
                'widget' => 'single_text',
            ])
            ->add('save', SubmitType::class, array('label' => 'Utwórz'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Payment::class,
            'csrf_protection' => false,
            'constraints' => new UniqueEntity([
                'fields' => ['name'],
                'message' => 'Taki rodzaj płatności już istnieje!',
            ]),
        ]);
    }
}
