<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Constraints\Regex;
use App\Entity\Ram;

class RamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = array(
            'required' => true,
        );
        $builder
        ->add('size', IntegerType::class, [
            'required' => true,
            'invalid_message' => 'Podałeś niewłaściwą pojemność (minimum %num% znaki) dla pamięci ram!',
            'invalid_message_parameters' => array('%num%' => 1),
            'constraints' => [
                new NotBlank([
                    'message' => "Pole pamięć ram nie powinno być puste!",
                ]),                
            ],
        ])
            ->add('save', SubmitType::class, array('label' => 'Utwórz'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ram::class,
            'csrf_protection' => false,
            'constraints' => new UniqueEntity([
                'fields' => ['size'],
                'message' => 'Ta wielkość pamięci RAM już istnieje!',
            ]),
        ]);
    }
}
