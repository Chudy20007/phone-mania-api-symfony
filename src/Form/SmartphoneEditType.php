<?php

namespace App\Form;

use App\Entity\Smartphone;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\CallbackValidator;
class SmartphoneEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = array(
            'required' => true,
        );
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole nazwa nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/[A-Za-z]{3,4}/',
                        'message' => 'Niepoprawna nazwa dla produktu!',
                    ]),
                ],
            ])
            ->add('price', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Podałeś niewłaściwą cenę dla produktu (minimum %num% znak)!',
                'invalid_message_parameters' => array('%num%' => 2),
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole cena nie powinno być puste!",
                    ]),
                ],
            ])
            ->add('count', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Podałeś niewłaściwą ilość (minimum %num% znak) produktów!',
                'invalid_message_parameters' => array('%num%' => 1),
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole ilość nie powinno być puste!",
                    ]),
                ],
            ])
            ->add('photo', TextType::class,[
                'required' => false,
            ])
      
            ->add('save', SubmitType::class, array('label' => 'Utwórz'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Smartphone::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
           // 'validation_groups' => false,
            'constraints' => new UniqueEntity([
                'fields' => ['name'],
                'message' => 'Smartfon o takiej wersji kolorystycznej już istnieje!',
            ]),
        ]);
    }
}
