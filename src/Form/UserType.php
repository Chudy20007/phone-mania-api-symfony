<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $required = array(
            'required' => true,
        );
        $builder
            ->add('first_name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole z imieniem nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/',
                        'message' => 'Niepoprawne imię!',
                    ]),
                ],
            ])
            ->add('last_name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole z nazwiskiem nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/',
                        'message' => 'Niepoprawne nazwisko!',
                    ]),
                ],
            ])
            ->add('email', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole z adres e-mail nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/^[0-9a-zA-Z_.-]+@[0-9a-zA-Z.-]+\.[a-zA-Z]{2,3}$/',
                        'message' => 'Niepoprawny adres e-mail!',
                    ]),
                ],
            ])
            ->add('post_code', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole kod pocztowy nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/^([0-9]{2})(-[0-9]{3})$/',
                        'message' => 'Niepoprawny kod pocztowy!',
                    ]),
                ],
            ])
            ->add('street', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole ulica nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/[A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{2,}/',
                        'message' => 'Niepoprawna nazwa dla ulicy!',
                    ]),
                ],
            ])
            ->add('street_number', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole numer ulicy nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/[A-ZĄĘĆŹŻŁŃa-ząęćńźżółu0-9]{2,}/',
                        'message' => 'Niepoprawny format dla numery ulicy!',
                    ]),
                ],
            ])
            ->add('city', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole miasto nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})/',
                        'message' => 'Niepoprawna nazwa dla miasta!',
                    ]),
                ],
            ])
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password')));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
            'constraints' => [
                new UniqueEntity([
                'fields' => ['email'],
                'message' => 'Adres e-mail jest już zajęty!',
            ]),
            ],
            'allow_extra_fields' => true,
        ]);
    }
}
