<?php

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole z komentarzem nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => "//^(([A-Z]{1}).{3,})//",
                        'message' => 'Niepoprawna komentarz dla artykułu!',
                    ]),
                ],
            ])
            ->add('user', EntityType::class, [
                'class' => User::class,
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole z użytkownikiem nie powinno być puste!",
                    ]),
                ],
                'invalid_message' => 'Podaj poprawny numer użytkownika!',
            ])
            ->add('article', EntityType::class, [
                'class' => Article::class,
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole z artykułem nie powinno być puste!",
                    ]),
                ],
                'invalid_message' => 'Podaj popranwy numer komentowanego artykułu!',

            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
            'cascade_validation' => true,
            'csrf_protection' => false,
            'csrf_field_name' => '_token',
            'allow_extra_fields' => true,
        ]);
    }
}
