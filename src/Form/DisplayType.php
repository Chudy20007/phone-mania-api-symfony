<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Constraints\Regex;
use App\Entity\Display;

class DisplayType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = array(
            'required' => true,
        );
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole nazwa nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/^([A-ZĄĘŹŻŁÓŁa-ząęźżół]{1,}.[A-ZĄĘŹŻŁÓŁa-ząęźżół]{1,})$/',
                        'message' => 'Niepoprawna nazwa dla wyświetlacza!',
                    ]),
                ],
            ])
            ->add('save', SubmitType::class, array('label' => 'Utwórz'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Display::class,
            'csrf_protection' => false,
            'constraints' => new UniqueEntity([
                'fields' => ['name'],
                'message' => 'Taki rodzaj wyświetlacza już istnieje!',
            ]),
        ]);
    }
}
