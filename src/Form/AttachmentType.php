<?php

namespace App\Form;

use App\Entity\Attachment;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\CallbackValidator;

class AttachmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('source', FileType::class,[
            'required' => true,
            'invalid_message' => 'Podałeś złe zdjęcie!',

            'constraints' => [
                new Regex([
                    'pattern' => '/[A-Za-z0-9]{3,4}/',
                    'message' => 'Niepoprawna nazwa dla zdjęcia!',
                ]),
                new NotBlank([
                    'message' => "Pole główne zdjęcie nie powinno być puste!",
                ]),
                
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Attachment::class,
            'csrf_protection' => false,

        ]);
    }
}
