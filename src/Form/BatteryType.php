<?php

namespace App\Form;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Battery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
 
class BatteryType extends AbstractType
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = array(
            'required' => true,
        );
        $builder
            ->add('type', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole typ nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/^([A-Z]{1}[a-z]{3,}[a-z]-[a-z]{3,})$/',
                        'message' => 'Niepoprawny typ dla baterii!'
                    ]),
                ],
            ])
            ->add('capacity', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Podałeś niewłaściwą pojemność (minimum %num% znaki) dla baterii!',
                'invalid_message_parameters' => array('%num%' => 4),
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole pojemność nie powinno być puste!",
                    ]),                
                ],
            ])
            ->add('save', SubmitType::class, array('label' => 'Utwórz'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Battery::class,
            'csrf_protection' => false,
            'constraints' => [
                
                new UniqueEntity([
                    'fields' => ['type'],
                    'message' => 'Taka bateria już istnieje!'
                    ]),
             new UniqueEntity([
                        'fields' => ['capacity'],
                        'message' => 'Taka caca już istnieje!'
                        ]),
                    ],
            'allow_extra_fields'=> true,
        ]);
    }
}
