<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Constraints\Regex;
use App\Entity\Camera;

class CameraType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = array(
            'required' => true,
        );
        $builder
            ->add('front', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Podałeś niewłaściwą wartość dla przedniego aparatu!',
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole aparat przedni nie powinno być puste!",
                    ]),
                ],
            ])
            ->add('back', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Podałeś niewłaściwą wartość dla tylniego aparatu!',
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole aparat tylni nie powinno być puste!",
                    ]),
                ],
            ])
            ->add('created_at', DateTimeType::class, [
                'invalid_message' => "Podana data jest niepoprawna!",
                'format' => 'yyyy-MM-dd HH:mm:ss',
                'input' => 'datetime',
                'widget' => 'single_text',
            ])
            ->add('save', SubmitType::class, array('label' => 'Utwórz'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Camera::class,
            'csrf_protection' => false,
            'constraints' => new UniqueEntity([
                'fields' => ['front', 'back'],
                'message' => 'Taki rodzaj aparatów już istnieje!',
            ]),
        ]);
    }
}
