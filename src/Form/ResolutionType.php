<?php

namespace App\Form;

use App\Entity\Resolution;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
 
class ResolutionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = array(
            'required' => true,
        );
        $builder
            ->add('width', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Podałeś niewłaściwą szerokość (minimum %num% znaki) dla wyświetlacza!',
                'invalid_message_parameters' => array('%num%' => 3),
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole szerokość nie powinno być puste!",
                    ]),                
                ],
            ])
            ->add('height', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Podałeś niewłaściwą wysokość (minimum %num% znaki) dla wyświetlacza!',
                'invalid_message_parameters' => array('%num%' => 3),
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole wysokość nie powinno być puste!",
                    ]),                
                ],
            ])
            ->add('save', SubmitType::class, array('label' => 'Utwórz'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Resolution::class,
            'csrf_protection' => false,
            'constraints' => new UniqueEntity([
                'fields' => ['width','height'],
                'message' => 'Taki rodzaj wyświetlacza już istnieje!'
                ])
        ]);
    }
}
