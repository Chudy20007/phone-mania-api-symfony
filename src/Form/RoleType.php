<?php

namespace App\Form;

use App\Entity\Role;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class RoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = array(
            'required' => true,
        );
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/([A-Z]{3,}_[A-Z]{3,})/',
                        'message' => 'Niepoprawna nazwa!']),
                ],
            ]); /*
    ->add('updated_at', DateTimeType::class,[
    'invalid_message' => "Podana data jest niepoprawna!",
    'format' => 'yyyy-MM-dd HH:mm:ss',
    'input' => 'datetime',
    'widget' => 'single_text',
    ])
    ->add('created_at', DateTimeType::class,[
    'invalid_message' => "Podana data jest niepoprawna!",
    'format' => 'yyyy-MM-dd HH:mm:ss',
    'input' => 'datetime',
    'widget' => 'single_text',
    ]);
     */
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Role::class,
            'csrf_protection' => false,
            'constraints' => [
                new UniqueEntity([
                'fields' => ['name'],
                'message' => 'Taka rola już istnieje!',
            ]),
            ],
            'allow_extra_fields' => true,
        ]);
    }

}
