<?php

namespace App\Form;

use App\Entity\Order;
use App\Entity\Payment;
use App\Form\OrderType;
use App\Validator\ContainsProducts;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole status zamówienia nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/[A-Za-ząęółźćż]{3,4}/',
                        'message' => 'Niepoprawna status zamówienia!',
                    ]),
                ],
            ])
            ->add('tax', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Niewłaściwa liczba określająca VAT (minimum %num% znak)!',
                'invalid_message_parameters' => array('%num%' => 2),
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole VAT nie powinno być puste!",
                    ]),
                ],
            ])
            ->add('discount', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Niewłaściwa liczba określająca VAT (minimum %num% znak)!',
                'invalid_message_parameters' => array('%num%' => 2),
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole VAT nie powinno być puste!",
                    ]),
                ],
            ])
            ->add('total_price', MoneyType::class, [
                'required' => true,
                'invalid_message' => 'Niewłaściwa liczba określająca cenę (minimum %num% znak)!',
                'invalid_message_parameters' => array('%num%' => 1),
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole koszt całkowity nie powinno być puste!",
                    ]),
                ],
            ])
            ->add('payment', EntityType::class, array(
                'required' => true,
                'class' => Payment::class))
            ->add('save', SubmitType::class, array('label' => 'Zamawiam i płacę'));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }
}
