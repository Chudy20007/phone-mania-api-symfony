<?php

namespace App\Form;

use App\Entity\Smartphone;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\CallbackValidator;
class SmartphoneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = array(
            'required' => true,
        );
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole nazwa nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/[A-Za-z]{3,4}/',
                        'message' => 'Niepoprawna nazwa dla produktu!',
                    ]),
                ],
            ])
            ->add('price', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Podałeś niewłaściwą cenę dla produktu (minimum %num% znak)!',
                'invalid_message_parameters' => array('%num%' => 2),
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole cena nie powinno być puste!",
                    ]),
                ],
            ])
            ->add('count', IntegerType::class, [
                'required' => true,
                'invalid_message' => 'Podałeś niewłaściwą ilość (minimum %num% znak) produktów!',
                'invalid_message_parameters' => array('%num%' => 1),
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole ilość nie powinno być puste!",
                    ]),
                ],
            ])
            ->add('photo', FileType::class,[
                'required' => true,
                'invalid_message' => 'Podałeś złe zdjęcie!',

                'constraints' => [
                    new Regex([
                        'pattern' => '/[A-Za-z0-9]{3,4}/',
                        'message' => 'Niepoprawna nazwa dla zdjęcia!',
                    ]),
                    new NotBlank([
                        'message' => "Pole główne zdjęcie nie powinno być puste!",
                    ]),
                    
                ],
            ])
            
            /*
        ->add('battery', IntegerType::class, [
        'required' => true,
        'invalid_message' => 'Podałeś niewłaściwy numer (minimum %num% znak) dla baterii!',
        'invalid_message_parameters' => array('%num%' => 1),
        'constraints' => [
        new NotBlank([
        'message' => "Pole bateria nie powinno być puste!",
        ]),
        ],
        ])
        /*    ->add('camera', IntegerType::class, [
        'required' => true,
        'invalid_message' => 'Podałeś niewłaściwy numer (minimum %num% znak) dla aparatu!',
        'invalid_message_parameters' => array('%num%' => 1),
        'constraints' => [
        new NotBlank([
        'message' => "Pole aparat nie powinno być puste!",
        ]),
        ],
        ])
        ->add('display', IntegerType::class, [
        'required' => true,
        'invalid_message' => 'Podałeś niewłaściwy numer (minimum %num% znak) dla wyświetlacza!',
        'invalid_message_parameters' => array('%num%' => 1),
        'constraints' => [
        new NotBlank([
        'message' => "Pole wyświetlacz nie powinno być puste!",
        ]),
        ],
        ])
        ->add('manufacturer', IntegerType::class, [
        'required' => true,
        'invalid_message' => 'Podałeś niewłaściwy numer (minimum %num% znak) dla producenta!',
        'invalid_message_parameters' => array('%num%' => 1),
        'constraints' => [
        new NotBlank([
        'message' => "Pole producent nie powinno być puste!",
        ]),
        ],
        ])
        ->add('memory', IntegerType::class, [
        'required' => true,
        'invalid_message' => 'Podałeś niewłaściwy numer (minimum %num% znak) dla pamięci flash!',
        'invalid_message_parameters' => array('%num%' => 1),
        'constraints' => [
        new NotBlank([
        'message' => "Pole pamięć flash nie powinno być puste!",
        ]),
        ],
        ])
        ->add('processor', IntegerType::class, [
        'required' => true,
        'invalid_message' => 'Podałeś niewłaściwy numer (minimum %num% znak) dla procesora!',
        'invalid_message_parameters' => array('%num%' => 1),
        'constraints' => [
        new NotBlank([
        'message' => "Pole procesor nie powinno być puste!",
        ]),
        ],
        ])
        ->add('ram', IntegerType::class, [
        'required' => true,
        'invalid_message' => 'Podałeś niewłaściwy numer (minimum %num% znak) dla pamięci RAM!',
        'invalid_message_parameters' => array('%num%' => 1),
        'constraints' => [
        new NotBlank([
        'message' => "Pole pamięć ram nie powinno być puste!",
        ]),
        ],
        ])
        ->add('resolution', IntegerType::class, [
        'required' => true,
        'invalid_message' => 'Podałeś niewłaściwy numer (minimum %num% znak) dla rozdzielczości!',
        'invalid_message_parameters' => array('%num%' => 1),
        'constraints' => [
        new NotBlank([
        'message' => "Pole rodzielczość nie powinno być puste!",
        ]),
        ],
        ])
        ->add('system', IntegerType::class, [
        'required' => true,
        'invalid_message' => 'Podałeś niewłaściwy numer (minimum %num% znak) dla systemu operacyjnego!',
        'invalid_message_parameters' => array('%num%' => 1),
        'constraints' => [
        new NotBlank([
        'message' => "Pole system operacyjny nie powinno być puste!",
        ]),
        ],
        ])
         */
            ->add('save', SubmitType::class, array('label' => 'Utwórz'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Smartphone::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
            'constraints' => new UniqueEntity([
                'fields' => ['name'],
                'message' => 'Smartfon o takiej wersji kolorystycznej już istnieje!',
            ]),
        ]);
    }
}
