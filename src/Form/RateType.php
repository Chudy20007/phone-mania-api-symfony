<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Rate;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class RateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rate', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole z oceną nie powinno być puste!",
                    ]),
                    new Regex([
                        'pattern' => '/([1-5]{1})/',
                        'message' => 'Niepoprawna ocena dla artykułu!',
                    ]),
                ],
            ])
            ->add('user', EntityType::class, [
                'class' => User::class,
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole z użytkownikiem nie powinno być puste!",
                    ]),
                ],
                'invalid_message' => 'Podaj poprawny numer użytkownika!',
            ])
            ->add('article', EntityType::class, [
                'class' => Article::class,
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Pole z artykułem nie powinno być puste!",
                    ]),
                ],
                'invalid_message' => 'Podaj popranwy numer artykułu!',

            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Rate::class,
            'cascade_validation' => true,
            'csrf_protection' => false,
            'csrf_field_name' => '_token',
            'allow_extra_fields' => true,
            'constraints' => new UniqueEntity([
                'fields' => ['user', 'article'],
                'message' => 'Taka ocena już istnieje!',
            ]),
        ]);
    }
}
