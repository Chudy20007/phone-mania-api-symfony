<?php

namespace App\Repository;

use App\Entity\SmartphoneAttachment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SmartphoneAttachment|null find($id, $lockMode = null, $lockVersion = null)
 * @method SmartphoneAttachment|null findOneBy(array $criteria, array $orderBy = null)
 * @method SmartphoneAttachment[]    findAll()
 * @method SmartphoneAttachment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmartphoneAttachmentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SmartphoneAttachment::class);
    }

    public function findRecords()
    {
        return $this->createQueryBuilder('r')
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function findActiveRecords()
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.deleted_at IS NULL')
            ->getQuery()
            ->getArrayResult()
        ;
    }
//    /**
//     * @return SmartphoneAttachment[] Returns an array of SmartphoneAttachment objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SmartphoneAttachment
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
