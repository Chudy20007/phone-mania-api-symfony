<?php

namespace App\Repository;

use Doctrine\ORM\Query\Expr;
use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function findRecords()
    {
        return $this->createQueryBuilder('o')
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function findActiveRecords()
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.deleted_at IS NULL')
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function findOrdersByUserID($userID)
    {
        return $this->createQueryBuilder('o')
            ->leftJoin('o.orderUsers', 'ou')
            ->leftJoin('o.payment', 'pa')
            ->leftJoin('ou.orderSmartphones','p')
            ->addSelect('pa')
            ->addSelect('ou')
      
            ->addSelect('o')
            ->andWhere('o.deleted_at IS NULL')
            ->andWhere('ou.user = :userID')
            ->setParameter('userID', $userID)
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function findActiveProductsInOrder($orderID)
    {
        return $this->createQueryBuilder('o')
        ->leftJoin('o.orderSmartphones', 'os', Expr\Join::WITH, 'os.deleted_at IS NULL')
        ->leftJoin('os.smartphone', 's')
        ->leftJoin('s.manufacturer', 'man')
        ->leftJoin('s.battery', 'b')
        ->leftJoin('s.system', 'sys')
        ->leftJoin('s.display', 'd')
        ->leftJoin('s.resolution', 'res')
        ->leftJoin('s.ram', 'r')
        ->leftJoin('s.memory', 'mem')
        ->leftJoin('s.camera', 'c')
        ->leftJoin('s.processor', 'p')
        ->addSelect('os', 'o','s', 'man', 'mem', 'd', 'c', 'res', 'r', 'sys', 'b', 'p')
        ->andWhere('o.id = :orderID')
        ->setParameter('orderID', $orderID)
        ->getQuery()
        ->getArrayResult()
    ;    
    }
    public function findOrderProducts($orderID)
    {
        return $this->createQueryBuilder('o')
            ->addSelect('o','pa','os','s')
            ->leftJoin('o.orderSmartphones', 'os', Expr\Join::WITH, 'os.deleted_at IS NULL')
            ->leftJoin('os.smartphone','s')
            ->leftJoin('o.payment', 'pa')


      
            ->andWhere('o.id = :orderID')
            ->andWhere('os.deleted_at IS NULL')
            ->setParameter('orderID', $orderID)
            ->getQuery()
            ->getResult()
        ;
    }



    public function deleteProductInOrder($orderID, $productID)
    {
        return $this->createQueryBuilder('o')
            ->leftJoin('o.orderSmartphones', 'os')
            ->andWhere('os.order = :orderID')
            ->andWhere('os.smartphone = :productID')
            ->setParameter('orderID', $orderID)
            ->setParameter('productID', $productID)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOrdersByUserID2($userID)
    {
        return $this->createQueryBuilder('o')
        ->andWhere('o.orderUsers = :val')
        ->setParameter('val', $userID)
        ->orderBy('o.id', 'ASC')
        ->setMaxResults(10)
        ->getQuery()
        ->getResult()
        ;
    }
//    /**
    //     * @return Order[] Returns an array of Order objects
    //     */
    /*
    public function findByExampleField($value)
    {
    return $this->createQueryBuilder('o')
    ->andWhere('o.exampleField = :val')
    ->setParameter('val', $value)
    ->orderBy('o.id', 'ASC')
    ->setMaxResults(10)
    ->getQuery()
    ->getResult()
    ;
    }
     */

    /*
public function findOneBySomeField($value): ?Order
{
return $this->createQueryBuilder('o')
->andWhere('o.exampleField = :val')
->setParameter('val', $value)
->getQuery()
->getOneOrNullResult()
;
}
 */
}
