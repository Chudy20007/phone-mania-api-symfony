<?php

namespace App\Repository;

use App\Entity\Smartphone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Smartphone|null find($id, $lockMode = null, $lockVersion = null)
 * @method Smartphone|null findOneBy(array $criteria, array $orderBy = null)
 * @method Smartphone[]    findAll()
 * @method Smartphone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmartphoneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Smartphone::class);
    }
    public function findProductByID($id)
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.manufacturer', 'man')
            ->leftJoin('s.battery', 'b')
            ->leftJoin('s.camera', 'c')
            ->leftJoin('s.system', 'sys')
            ->leftJoin('s.display', 'd')
            ->leftJoin('s.resolution', 'res')
            ->leftJoin('s.ram', 'r')
            ->leftJoin('s.memory', 'mem')
            ->leftJoin('s.processor', 'p')
            ->leftJoin('s.smartphoneAttachments', 'sa')
            ->leftJoin('sa.attachment', 'a')
            ->addSelect('man', 'mem', 'd', 'c', 'a', 'res', 'sa', 'r', 'sys', 'b', 'p')
            ->andWhere('s.id = :id')
            ->andWhere('s.deleted_at IS NULL')
            ->setParameter('id', $id)
            ->getQuery()
            ->getArrayResult();

    }

    public function findActiveRecordsByManufacturers($manufacturers)
    {
        $counter = 0;
        $q = $this->createQueryBuilder('s')
            ->leftJoin('s.manufacturer', 'man')
            ->leftJoin('s.battery', 'b')
            ->leftJoin('s.system', 'sys')
            ->leftJoin('s.display', 'd')
            ->leftJoin('s.resolution', 'res')
            ->leftJoin('s.ram', 'r')
            ->leftJoin('s.memory', 'mem')
            ->leftJoin('s.camera', 'c')
            ->leftJoin('s.processor', 'p')
            ->addSelect('man', 'mem', 'd', 'c', 'res', 'r', 'sys', 'b', 'p');

        foreach ($manufacturers as $manufacturer) {
            $q->orWhere('man.name = :manufacturer' . $counter)
                ->setParameter('manufacturer' . $counter, $manufacturer);
            $counter++;
        }
        $q->andWhere('s.deleted_at IS NULL');
        return $q
            ->getQuery()
            ->getArrayResult();
    }

    public function findActiveRecordsByValue($value)
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.manufacturer', 'man')
            ->leftJoin('s.battery', 'b')
            ->leftJoin('s.system', 'sys')
            ->leftJoin('s.display', 'd')
            ->leftJoin('s.resolution', 'res')
            ->leftJoin('s.ram', 'r')
            ->leftJoin('s.memory', 'mem')
            ->leftJoin('s.camera', 'c')
            ->leftJoin('s.processor', 'p')
            ->addSelect('man', 'mem', 'd', 'c', 'res', 'r', 'sys', 'b', 'p')
            ->where('s.name LIKE :value')
            ->orWhere('man.name LIKE :value')
            ->andWhere('s.deleted_at IS NULL')

            ->setParameter('value', '%'.$value.'%')
            ->getQuery()
            ->getArrayResult();
    }


    public function findActiveRecords()
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.manufacturer', 'man')
            ->leftJoin('s.battery', 'b')
            ->leftJoin('s.system', 'sys')
            ->leftJoin('s.display', 'd')
            ->leftJoin('s.resolution', 'res')
            ->leftJoin('s.ram', 'r')
            ->leftJoin('s.memory', 'mem')
            ->leftJoin('s.camera', 'c')
            ->leftJoin('s.processor', 'p')
            ->addSelect('man', 'mem', 'd', 'c', 'res', 'r', 'sys', 'b', 'p')
            ->andWhere('s.deleted_at IS NULL')
            ->getQuery()
            ->getArrayResult();

    }

    public function findActiveRecordsWithActiveAttachments($productID)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.deleted_at IS NULL')
            ->leftJoin('s.smartphoneAttachments', 'sa')
            ->leftJoin('s.resolution', 'resolution')

            ->addSelect('sa')
            ->addSelect('resolution')
            ->andWhere('s.deleted_at IS NULL')
            ->andWhere('sa.deleted_at IS NULL')
            ->andWhere('s.id = :id')
        //  ->andWhere('resolution.deleted_at IS NULL')
            ->setParameter('id', $productID)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

//    /**
    //     * @return Smartphone[] Returns an array of Smartphone objects
    //     */
    /*
    public function findByExampleField($value)
    {
    return $this->createQueryBuilder('s')
    ->andWhere('s.exampleField = :val')
    ->setParameter('val', $value)
    ->orderBy('s.id', 'ASC')
    ->setMaxResults(10)
    ->getQuery()
    ->getResult()
    ;
    }
     */

    /*
public function findOneBySomeField($value): ?Smartphone
{
return $this->createQueryBuilder('s')
->andWhere('s.exampleField = :val')
->setParameter('val', $value)
->getQuery()
->getOneOrNullResult()
;
}
 */
}
