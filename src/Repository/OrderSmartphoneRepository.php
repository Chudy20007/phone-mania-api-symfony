<?php

namespace App\Repository;

use App\Entity\OrderSmartphone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OrderSmartphone|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderSmartphone|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderSmartphone[]    findAll()
 * @method OrderSmartphone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderSmartphoneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OrderSmartphone::class);
    }

    public function findRecords()
    {
        return $this->createQueryBuilder('o')
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function findActiveRecords()
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.deleted_at IS NULL')
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function findOrderProducts($orderID)
    {
        return $this->createQueryBuilder('o')
            ->leftJoin('o.smartphone','s')

            ->addSelect('o')

            ->addSelect('s')
      
            ->andWhere('o.order = :orderID')
            ->setParameter('orderID', $orderID)
            ->getQuery()
            ->getResult()
        ;
    }


//    /**
//     * @return OrderSmartphone[] Returns an array of OrderSmartphone objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderSmartphone
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
