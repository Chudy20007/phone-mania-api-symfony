<?php

namespace App\Security\Voter;

use App\Entity\Order;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class OrderVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const CREATE = 'create';
    const DELETE = 'delete';
    const CANCEL = 'cancel';
    const DELETE_PRODUCT ='deleteProductFromOrder';
    
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPPORT = 'ROLE_SUPPORT';
    const ROLE_WORKER = 'ROLE_WORKER';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::VIEW, self::EDIT, self::CREATE, self::DELETE_PRODUCT, self::CANCEL, self::DELETE))) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token;

        if (null !== $token) {
            $user = $token->getUser();
        }

        $order = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($order, $user);
                break;
            case self::VIEW:
                return $this->canView($order, $user);
                break;
            case self::DELETE:
                return $this->canDelete($user);
                break;
            case self::CANCEL:
                return $this->canCancel($order, $user);
                break;
            case self::CREATE:
                return $this->canCreate($order, $user);
                break;
            case self::DELETE_PRODUCT:
                return $this->canDelete($user);
                break;
            
        }

        return false;
    }

    private function canCreate(User $user)
    {
        return ($user->getRole() !== null);
    }

    private function canCancel(Order $order, User $user)
    {
        return ($user->getId() === $order->getOwner()->getId() && $order->getStatus !== 'zakończono' || $user->getRole()->getName() === self::ROLE_ADMIN);
    }

    private function canDelete(User $user)
    {
        return ($user->getRole()->getName() === self::ROLE_ADMIN && $user->getRole()->getName() === self::ROLE_SUPPORT);
    }

    private function canView(Order $order, User $user)
    {
        return ($user->getId() === $order->getOwner()->getId() || $user->getRole()->getName() === self::ROLE_ADMIN);
    }

    private function canEdit(Order $order, User $user)
    {
        return ($user->getId() === $order->getOwner()->getId() && $order->getStatus !== 'zakończono' || $user->getRole()->getName() === self::ROLE_ADMIN);
    }
}
