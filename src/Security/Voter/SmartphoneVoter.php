<?php

namespace App\Security\Voter;

use App\Entity\Smartphone;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SmartphoneVoter extends Voter
{
    const EDIT = 'edit';
    const DELETE = 'delete';
    const VIEW = 'delete';

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPPORT = 'ROLE_SUPPORT';
    const ROLE_WORKER = 'ROLE_WORKER';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::EDIT, self::DELETE, self::VIEW))) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token;

        if (null !== $token) {
            $user = $token->getUser();
        }

        $battery = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($smartphone, $user);
                break;
            case self::DELETE:
                return $this->canDelete($smartphone, $user);
                break;
            case self::VIEW:
                return $this->canView($smartphone, $user);
                break;
        }

        return false;
    }

    private function canDelete(Smartphone $smartphone, User $user)
    {
        return ($user->getRole() === self::ROLE_ADMIN || $user->getRole() === self::ROLE_WORKER);
    }

    private function canView(Smartphone $smartphone, User $user)
    {
        return ($user->getRole() === self::ROLE_ADMIN || $user->getRole() === self::ROLE_WORKER || $user->getRole() === self::ROLE_SUPPORT);
    }

    private function canEdit(Smartphone $smartphone, User $user)
    {
        return ($user->getRole() === self::ROLE_ADMIN || $user->getRole() === self::ROLE_WORKER);
    }
}
