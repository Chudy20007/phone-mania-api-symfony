<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Repository\VerifyUserRepository;
use App\Entity\VerifyUser;
use App\Repository\ForgotPasswordRepository;
use App\Entity\ForgotPassword;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class DeleteExpiredTokensCommand extends ContainerAwareCommand 
{
    protected static $defaultName = 'app:delete-expired-tokens';

    protected function configure()
    {
        $this
        ->setDescription('Delete expired tokens from the VerifyUser and ForgotPassword tables in the DB.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $em = $this->getContainer()->get("doctrine")->getManager();
            $date = new \DateTime(date('Y-m-d H:i:s'));
            $count = $em
            ->getRepository(VerifyUser::class)
            ->deleteExpiredTokens(7,$date);
            $count2 = $em
            ->getRepository(ForgotPassword::class)
            ->deleteExpiredTokens(7,$date);
        }
        catch(\Symfony\Component\HttpKernel\Exception $e){
            $io->error($e->getMessage());
        } 
        $io->success('Deleted '.$count.' rows!');    
        $io->success('Deleted '.$count2.' rows!');       
    }

}
