<p align="center"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSz2wbGv6LU2Y9KpnISoAEsvDxl3Fk-L130opgjnOsL6lDp4jm6"></p>

<p align="center">
<img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Introduction

Phone Mania API is an application based on the framework Symfony. This API allows you to manage smartphones shop/wiki.

## Features

*  CRUD for the all controllers with data validation (requests, validators)
*  Full authorization with authentication (validators, policies, security middlewares)
*  Security against: CORS, CSRF , XSS, SQL Injection, Bots
*  Authorization type:  Authorization Bearer
*  User interface: recovering and resetting password, account verification

## Used Components

* Voters
* ACL
* Repositories
* Forms
* Annotations
* Mail
* Services
* Enities
* Migrations
* Data Fixtures
* Commands (delete expired tokens from verify users table)

## Used packages

* [LexikJWTAuthenticationBundle - MIT](https://github.com/lexik/LexikJWTAuthenticationBundle)
* [NelmioCorsBundle- MIT](https://github.com/nelmio/NelmioCorsBundle)


## Installation

1.  Clone the project: `git clone https://github.com/Chudy20007/phone-mania-api-symfony`.
2.  In the CLI (composer) pass this command: `composer install `. 
3.  Generate the SSH keys :
`mkdir -p config/jwt # For Symfony3+, no need of the -p option`
`openssl genrsa -out config/jwt/private.pem -aes256 4096`
`openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem`
4.  Create a database and configure **.env** file (details in the next subsection).
5.  Run the migrations with seeders (important! don't change the values 'names' in the 'roles' table): `php bin/console doctrine:migrations:migrate` , `php bin/console doctrine:fixtures:load`.
6.  Run application: `php bin/console server:run`.

## Configuration

*You must specify environment variables in the **.env** file.*

Example:
1. APP_VUE_URL=http://phonemania.pl/api - specify Frontend APP URL
2. APP_EMAIL=youremail@gmail.com
3. JWT_PUBLIC_KEY=jwt/public.pem - generate jwt public key with OpenSSH (**remember => generate key to the public.pem file!**)
4. JWT_SECRET_KEY=jwt/private.pem - generate jwt private key with OpenSSH (**remember => generate key to the private.pem file!**)
5. JWT_PASSPHRASE - specify jwt passphrase
6. CORS_ALLOW_ORIGIN=^https?://localhost(:[0-9]+)?$

If you have problem with configure .env file you can check **.env-example** file.

**Generate jwt secret key**

Set the JWTAuth secret key used to sign the tokens: `php bin/console lexik:jwt:generate-token`
This will update your .env file with something like JWT_SECRET=foobar

For more information check the package docs: [text](https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md#installation)


## Available addresses in the API

<table style='width:100%'>
    <tr>
        <td width='10%'>
            <h4>HTTP Method</h4>
        </td>
        <td width='10%'>
            <h4>Route</h4>
        </td>
        <td width='80%'>
            <h4>Corresponding Action</h4>
        </td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/user-orders</td>
        <td>App\Controller\OrderController::userOrders</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/orders</td>
        <td>App\Controller\OrderController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/orders/create</td>
        <td>App\Controller\OrderController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/orders</td>
        <td>App\Controller\OrderController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/orders/{order}</td>
        <td>App\Controller\OrderController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/orders/{order}/edit</td>
        <td>App\Controller\OrderController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/orders/{order}</td>
        <td>App\Controller\OrderController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/orders/{order}</td>
        <td>App\Controller\OrderController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rates</td>
        <td>App\Controller\RateController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rates/create</td>
        <td>App\Controller\RateController::create</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rates/{rate}</td>
        <td>App\Controller\RateController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rates/{rate}/edit</td>
        <td>App\Controller\RateController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/rates/{rate}</td>
        <td>App\Controller\RateController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/rates/{rate}</td>
        <td>App\Controller\RateController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/smartphones/create</td>
        <td>App\Controller\SmartphoneController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/smartphones</td>
        <td>App\Controller\SmartphoneController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/smartphones/{smartphone}/edit</td>
        <td>App\Controller\SmartphoneController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/smartphones/{smartphone}</td>
        <td>App\Controller\SmartphonesController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/smartphones/{smartphone}</td>
        <td>App\Controller\SmartphoneController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/manufacturers/create</td>
        <td>App\Controller\ManufacturerController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/manufacturers</td>
        <td>App\Controller\ManufacturerController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/manufacturers/{manufacturer}</td>
        <td>App\Controller\ManufacturerController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/manufacturers/{manufacturer}/edit</td>
        <td>App\Controller\ManufacturerController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/manufacturers/{manufacturer}</td>
        <td>App\Controller\ManufacturerController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/manufacturers/{manufacturer}</td>
        <td>App\Controller\ManufacturerController::destroy</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/orders/{id}/update-status</td>
        <td>App\Controller\OrderController::updateOrderStatus</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/users</td>
        <td>App\Controller\UserController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/users/create</td>
        <td>App\Controller\UserController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/users</td>
        <td>App\Controller\UserController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/users/{user}</td>
        <td>App\Controller\UserController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/users/{user}/edit</td>
        <td>App\Controller\UserController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/users/{user}</td>
        <td>App\Controller\UsersController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/users/{user}</td>
        <td>App\Controller\UserController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/roles</td>
        <td>App\Controller\RoleController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/roles/create</td>
        <td>App\Controller\RoleController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/roles</td>
        <td>App\Controller\RoleController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/roles/{role}</td>
        <td>App\Controller\RolesController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/roles/{role}/edit</td>
        <td>App\Controller\RoleController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/roles/{role}</td>
        <td>App\Controller\RoleController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/roles/{role}</td>
        <td>App\Controller\RoleController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/memories</td>
        <td>App\Controller\MemoryController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/memories/create</td>
        <td>App\Controller\MemoryController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/memories</td>
        <td>App\Controller\MemoryController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/memories/{memory}</td>
        <td>App\Controller\MemoryController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/memories/{memory}/edit</td>
        <td>App\Controller\MemoryController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/memories/{memory}</td>
        <td>App\Controller\MemoryController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/memories/{memory}</td>
        <td>App\Controller\MemoryController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/tags</td>
        <td>App\Controller\TagController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/tags/create</td>
        <td>App\Controller\TagController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/tags</td>
        <td>App\Controller\TagController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/tags/{tag}</td>
        <td>App\Controller\TagController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/tags/{tag}/edit</td>
        <td>App\Controller\TagController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/tags/{tag}</td>
        <td>App\Controller\TagController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/tags/{tag}</td>
        <td>App\Controller\TagController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/cameras</td>
        <td>App\Controller\CameraController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/cameras/create</td>
        <td>App\Controller\CameraController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/cameras</td>
        <td>App\Controller\CameraController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/cameras/{camera}</td>
        <td>App\Controller\CameraController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/cameras/{camera}/edit</td>
        <td>App\Controller\CameraController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/cameras/{camera}</td>
        <td>App\Controller\CameraController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/cameras/{camera}</td>
        <td>App\Controller\CameraController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rams</td>
        <td>App\Controller\RamController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rams/create</td>
        <td>App\Controller\RamController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/rams</td>
        <td>App\Controller\RamController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rams/{ram}</td>
        <td>App\Controller\RamController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rams/{ram}/edit</td>
        <td>App\Controller\RamController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/rams/{ram}</td>
        <td>App\Controller\RamController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/rams/{ram}</td>
        <td>App\Controller\RamController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/resolutions</td>
        <td>App\Controller\ResolutionController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/resolutions/create</td>
        <td>App\Controller\ResolutionController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/resolutions</td>
        <td>App\Controller\ResolutionController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/resolutions/{resolution}</td>
        <td>App\Controller\ResolutionController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/resolutions/{resolution}/edit</td>
        <td>App\Controller\ResolutionController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/resolutions/{resolution}</td>
        <td>App\Controller\ResolutionController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/resolutions/{resolution}</td>
        <td>App\Controller\ResolutionsController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/displays</td>
        <td>App\Controller\DisplayController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/displays/create</td>
        <td>App\Controller\DisplayController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/displays</td>
        <td>App\Controller\DisplayController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/displays/{display}</td>
        <td>App\Controller\DisplayController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/displays/{display}/edit</td>
        <td>App\Controller\DisplayController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/displays/{display}</td>
        <td>App\Controller\DisplayController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/displays/{display}</td>
        <td>App\Controller\DisplayController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/batteries</td>
        <td>App\Controller\BatteriesController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/batteries/create</td>
        <td>App\Controller\BatteryController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/batteries</td>
        <td>App\Controller\BatteryController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/batteries/{battery}</td>
        <td>App\Controller\BatteryController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/batteries/{battery}/edit</td>
        <td>App\Controller\BatteryController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/batteries/{battery}</td>
        <td>App\Controller\BatteryController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/batteries/{battery}</td>
        <td>App\Controller\BatteryController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/processors</td>
        <td>App\Controller\ProcessorController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/processors/create</td>
        <td>App\Controller\ProcessorController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/processors</td>
        <td>App\Controller\ProcessorController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/processors/{processor}</td>
        <td>App\Controller\ProcessorController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/processors/{processor}/edit</td>
        <td>App\Controller\ProcessorController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/processors/{processor}</td>
        <td>App\Controller\ProcessorController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/processors/{processor}</td>
        <td>App\Controller\ProcessorController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/systems</td>
        <td>App\Controller\SystemController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/systems/create</td>
        <td>App\Controller\SystemController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/systems</td>
        <td>App\Controller\SystemController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/systems/{system}</td>
        <td>App\Controller\SystemController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/systems/{system}/edit</td>
        <td>App\Controller\SystemController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/systems/{system}</td>
        <td>App\Controller\SystemController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/systems/{system}</td>
        <td>App\Controller\SystemsController::destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/payments</td>
        <td>App\Controllers\PaymentController::index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/payments/create</td>
        <td>App\Controller\PaymentController::create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/payments</td>
        <td>App\Controller\PaymentController::store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/payments/{payment}</td>
        <td>App\Controller\PaymentController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/payments/{payment}/edit</td>
        <td>App\Controller\PaymentController::edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/payments/{payment}</td>
        <td>App\Controller\PaymentController::update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/payments/{payment}</td>
        <td>App\Controller\PaymentController::destroy</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/orders/{orderID}/delete/{productID}</td>
        <td>App\Controller\OrderController::deleteProductFromOrder</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/logout</td>
        <td>App\Controller\UserController::logout</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/routes</td>
        <td>Closure</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/smartphones/{id}</td>
        <td>App\Controller\SmartphoneController::show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/manufacturers</td>
        <td>App\Controller\ManufacturerController::index</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/find-products-by-manufacturers</td>
        <td>App\Controller\SmartphoneController::findProductsByManufacturers</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/smartphones</td>
        <td>App\Controller\SmartphoneController::index</td>
    </tr>
    <tr>
        <td>ANY</td>
        <td>api/login</td>
        <td>---</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/login_check</td>
        <td>----</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/find/{value}</td>
        <td>App\Controller\SmartphonesController::findProducts</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/storage/app/public/img/photos/{img}</td>
        <td>App\Controller\SmartphonesController::getPhotos</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/refresh</td>
        <td>App\Controller\AuthController::refresh</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/user/verify/{token}</td>
        <td>App\Controller\UserController::verifyAccount</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/email</td>
        <td>App\Controller\UserController::forgotPassword</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/reset</td>
        <td>App\Controller\UserController::resetPassword</td>
    </tr>
</table>


## License
"Phone Mania API" is an open-sourced software licensed under the MIT license(https://opensource.org/licenses/MIT).
