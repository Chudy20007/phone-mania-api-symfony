<div class='row text-center'>
    <div style='border-bottom:2px solid gray' class='col-md-12 col-lg-12 col-xs-12'>
<img style='width:20px; height:20px' src="{{URL::asset('/css/img/logo.png')}}"> Platforma e-learningowa</img> <br/> Dokończenie rejestracji konta
    </div>
<br/>
Wysłaliśmy do ciebie link aktywacyjny do twojego konta na platformie e-learningowej.
<div class='col-md-12 col-xs-12'>

     Proszę, kliknij w poniższy link, aby dokończyć proces rejestracji.
     <br/>
<a href="{{url('activate_account',$token)}}">Aktywuj konto</a>
    </div>
    <br/>
    <div style='border-top:2px solid gray' class='col-md-12 col-lg-12 col-xs-12'>
    Pozdrawiamy, <br/>
    Ekipa platformy e-learningowej
    </div>
</div>
